//
//  SignupTestCase.m
//  iSignin
//
//  Created by Damon Yuan on 2/4/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <KIF/KIF.h>
#import "KIFUITestActor+EXAdditions.h"

@interface SignupTestCase : KIFTestCase

@end

@implementation SignupTestCase

- (void)beforeEach
{
    [tester waitAndDismissUpdateAvailableAlert];
    [tester navigateToSignupPage];
}

- (void)afterEach
{
    [tester tryFindingViewWithAccessibilityLabel:NSLocalizedString(@"Congratulations!", @"ISSignupViewController")
                                         timeout:20.f];
}

- (void)testSuccessfulLogin
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyyMMddhhmmss";
    NSDate *date        = [NSDate date];
    NSString *name      = [NSString stringWithFormat:@"UserExample-%@", [dateFormatter stringFromDate:date]];
    NSString *email     = [NSString stringWithFormat:@"%@@example.com", name];
    
    [tester enterText:email intoViewWithAccessibilityLabel:@"Email text field"];
    [tester enterText:name intoViewWithAccessibilityLabel:@"Username text field"];
    [tester enterText:@"123456test" intoViewWithAccessibilityLabel:@"Password text field"];
    [tester enterText:@"123456test" intoViewWithAccessibilityLabel:@"Confirmation text field"];
    [tester tapViewWithAccessibilityLabel:@"Sign up button"];
}

@end
