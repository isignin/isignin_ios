//
//  LoginTest.m
//  iSignin
//
//  Created by Damon Yuan on 2/4/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <KIF/KIF.h>
#import "KIFUITestActor+EXAdditions.h"

@interface LoginTestCase : KIFTestCase

@end

@implementation LoginTestCase

- (void)beforeEach
{
    [tester waitAndDismissUpdateAvailableAlert];
    if ([tester tryFindingTappableViewWithAccessibilityLabel:@"Sign in button" timeout:5.f]) {
        [tester returnToLoggedOutHomeScreen];
    } else {
        [tester navigateToLoginPage];
    }
}

- (void)afterEach
{
    [tester returnToLoggedOutHomeScreen];
}

- (void)testSuccessfulLogin
{
    [tester enterText:@"damon.yuan@bindo.com" intoViewWithAccessibilityLabel:@"Email Username text field"];
    [tester enterText:@"111111" intoViewWithAccessibilityLabel:@"Password text field"];
    [tester tapViewWithAccessibilityLabel:@"Sign in button"];
    [tester tryFindingViewWithAccessibilityLabel:@"Left Nav Button"
                                         timeout:10.f];
}

@end
