//
//  KIFUITestActor+EXAdditions.h
//  iSignin
//
//  Created by Damon Yuan on 2/4/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "KIFUITestActor.h"

@interface KIFUITestActor (EXAdditions)

- (BOOL)tryFindingViewWithAccessibilityLabel:(NSString *)label timeout:(NSTimeInterval)timeout;
- (BOOL)tryFindingTappableViewWithAccessibilityLabel:(NSString *)label timeout:(NSTimeInterval)timeout;
- (void)tryTapViewWithAccessibilityLabel:(NSString *)label;
- (void)waitAndDismissUpdateAvailableAlert;

- (void)navigateToLoginPage;
- (void)navigateToSignupPage;
- (void)returnToLoggedOutHomeScreen;

- (void)selectTableViewWithLabel:(NSString *)label
                         AtIndex:(NSIndexPath *)indexPath;


@end
