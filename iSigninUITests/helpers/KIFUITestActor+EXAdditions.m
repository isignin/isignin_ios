//
//  KIFUITestActor+EXAdditions.m
//  iSignin
//
//  Created by Damon Yuan on 2/4/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "KIFUITestActor+EXAdditions.h"
#import "UIAccessibilityElement-KIFAdditions.h"

@implementation KIFUITestActor (EXAdditions)

- (void)waitAndDismissUpdateAvailableAlert
{
    if ([self tryFindingViewWithAccessibilityLabel:@"Update available" timeout:20.0]) {
        [self tapViewWithAccessibilityLabel:@"Ignore"];
    }
}

- (BOOL)tryFindingViewWithAccessibilityLabel:(NSString *)label
                                     timeout:(NSTimeInterval)timeout
{
    return [self tryRunningBlock:^KIFTestStepResult(NSError *__autoreleasing *error) {
        return [UIAccessibilityElement accessibilityElement:NULL
                                                       view:NULL
                                                  withLabel:label
                                                      value:nil
                                                     traits:UIAccessibilityTraitNone
                                                   tappable:NO error:error] ? KIFTestStepResultSuccess : KIFTestStepResultWait;
    }
                        complete:nil
                         timeout:timeout
                           error:nil];
}

- (BOOL)tryFindingTappableViewWithAccessibilityLabel:(NSString *)label
                                             timeout:(NSTimeInterval)timeout
{
    return [self tryRunningBlock:^KIFTestStepResult(NSError *__autoreleasing *error) {
        return [UIAccessibilityElement accessibilityElement:NULL
                                                       view:NULL
                                                  withLabel:label
                                                      value:nil
                                                     traits:UIAccessibilityTraitNone
                                                   tappable:YES error:error] ? KIFTestStepResultSuccess : KIFTestStepResultWait;
    }
                        complete:nil
                         timeout:timeout
                           error:nil];
}

- (void)tryTapViewWithAccessibilityLabel:(NSString *)label
{
    if ([self tryFindingTappableViewWithAccessibilityLabel:label timeout:10.0]) {
        [self tapViewWithAccessibilityLabel:label];
    }
}

- (void)navigateToLoginPage
{
    [self tapViewWithAccessibilityLabel:@"Sign In"];
}

- (void)navigateToSignupPage
{
    [self tapViewWithAccessibilityLabel:@"Sign Up"];
}

- (void)returnToLoggedOutHomeScreen
{
    [tester tapViewWithAccessibilityLabel:@"Left Nav Button"];
    [tester tapRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0] inTableViewWithAccessibilityIdentifier:@"Menu List"];
    
}

- (void)selectTableViewWithLabel:(NSString *)label
                         AtIndex:(NSIndexPath *)indexPath
{
    [tester tapRowAtIndexPath:indexPath inTableViewWithAccessibilityIdentifier:label];
    [tester waitForAbsenceOfViewWithAccessibilityLabel:label];
}

@end
