require 'rubygems'

namespace "bwoken" do
  require 'bwoken/tasks'
end

Bwoken.instance_eval do
  def app_name
    "iSignin"
  end

  def workspace_or_project_flag
    ws = xcworkspace
    if File.exists?(ws)
      "-workspace '#{ws}'"
    else
     "-project '#{xcodeproj}'"
    end
  end
end

