require 'rubygems'
require 'plist'
INFO_PLIST ="iSignin/Support Files/Info-Release.plist"
INFO_PLIST_DEV ="iSignin/Support Files/Info-Dev.plist"
INFO_PLIST_BETA="iSignin/Support Files/Info-Beta.plist"

task :is_clean do
  git_status = `git status --porcelain`
  result = git_status.to_s.empty?
  abort "there are files you need commit first" if !result
end

namespace :bump do
  desc "bump dev"
  task :dev => :is_clean do
    infos_dev = Plist::parse_xml(INFO_PLIST_DEV)
    build = infos_dev['CFBundleVersion']
    version = infos_dev['CFBundleShortVersionString']
    build = build.to_i + 1
    infos_dev["CFBundleVersion"] = build.to_s
    File.open(INFO_PLIST_DEV, "w") do |f|
      f.puts Plist::Emit.dump infos_dev, false
    end
    `git commit -am 'bump dev version to v#{version}.#{build}'`
    `git push`
  end
  
  desc "bump beta"
  task :beta => :is_clean do
      infos_beta = Plist::parse_xml(INFO_PLIST_BETA)
      build = infos_beta['CFBundleVersion']
      version = infos_beta['CFBundleShortVersionString']
      build = build.to_i + 1
      infos_beta["CFBundleVersion"] = build.to_s
      File.open(INFO_PLIST_BETA, "w") do |f|
          f.puts Plist::Emit.dump infos_beta, false
      end
      `git commit -am 'bump beta version to v#{version}.#{build}'`
      `git push`
  end

  desc "bump production"
  task :production => :is_clean do
    infos = Plist::parse_xml(INFO_PLIST)
    build = infos['CFBundleVersion']
    version = infos['CFBundleShortVersionString']
    build = build.to_i + 1
    infos["CFBundleVersion"] = build.to_s
    File.open(INFO_PLIST, "w") do |f|
      f.puts Plist::Emit.dump infos, false
    end
    `git commit -am 'bump production version to v#{version}.#{build}'`
    `git push`
  end
end
