//
//  ISWelcomeViewController.m
//  iSignin
//
//  Created by Damon Yuan on 22/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISWelcomeViewController.h"
#import "ISSigninViewController.h"
#import "ISSignupViewController.h"
#import "MZFormSheetController.h"
#import "ISPageViewController.h"
#import "ISEasingFunction.h"
#import "ISParallaxPageLayouter.h"
#import "ISPageChildViewController.h"
#import "ISShimmerLogoView.h"
#import "SKSplashIcon.h"

@interface ISWelcomeViewController()
<
    ISPageViewControllerDataSource,
    ISPageViewControllerDelegate,
    UIGestureRecognizerDelegate
>

@property (nonatomic, strong) ISPageViewController *pageViewController;
@property (nonatomic, strong) NSMutableDictionary  *viewControllerCache;
@property (nonatomic, strong) FUIButton            *signinButton;
@property (nonatomic, strong) FUIButton            *signupButton;
@property (nonatomic, strong) UIPageControl        *pageControl;
@property (nonatomic, strong) NSTimer              *timer;
@property (nonatomic, strong) UIImageView          *bgImageView;
@property (nonatomic, strong) UIView               *buttonBgView;
@property (nonatomic, strong) ISShimmerLogoView    *logoView;
@property (nonatomic, strong) UIVisualEffectView   *blurView;
@property (nonatomic, strong) SKSplashView         *splashView;
@property (nonatomic, assign) BOOL                 isSplash;

@end

@implementation ISWelcomeViewController

- (void)dealloc
{
    [_timer invalidate];
    _timer                         = nil;
    _pageViewController.delegate   = nil;
    _pageViewController.dataSource = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithSplash:(BOOL)isSplash
{
    if (self = [self init]) {
        _isSplash = isSplash;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self _setupLayout];
    if (self.isSplash) {
        [self _splash];
    }
}

- (void)_setupLayout
{
    self.title = NSLocalizedString(@"Welcome to iSignin", @"ISWelcomeViewController");
    
    [self.view setBackgroundColor:[ISAppStyleSheet defaultBackgroundColor]];
    
    [self.view addSubview:self.bgImageView];
    [self.bgImageView addSubview:self.blurView];
    [self.blurView setHidden:YES];
    
    [self addAndDisplayChildViewController:self.pageViewController];
    [self.pageViewController setLayouter:[[ISParallaxPageLayouter alloc] init]
                                animated:YES
                              completion:nil];
    [self.pageViewController setEasingFunction:[ISEasingFunction easingFunctionWithType:ISEasingFunctionTypeCircularEaseInOut]];
    [self.pageViewController setAnimationDuration:1.f];
    [self.pageViewController setContinuousNavigationEnabled:YES];
    
    [self.view addSubview:self.pageControl];
    [self.view addSubview:self.buttonBgView];
    [self.buttonBgView addSubview:self.signinButton];
    [self.buttonBgView addSubview:self.signupButton];
    if (IS_PAD) {
        [self.buttonBgView addSubview:self.logoView];
    }
    
    self.pageViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
    self.buttonBgView.translatesAutoresizingMaskIntoConstraints            = NO;
    self.signupButton.translatesAutoresizingMaskIntoConstraints            = NO;
    self.signinButton.translatesAutoresizingMaskIntoConstraints            = NO;
    self.pageControl.translatesAutoresizingMaskIntoConstraints             = NO;
    self.bgImageView.translatesAutoresizingMaskIntoConstraints             = NO;
    self.blurView.translatesAutoresizingMaskIntoConstraints                = NO;
    if (IS_PAD) {
        self.logoView.translatesAutoresizingMaskIntoConstraints            = NO;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resetTimer)
                                                 name:kISWelcomeViewShouldResetTimer
                                               object:nil];
    
    [self.view setNeedsUpdateConstraints];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self performSelector:@selector(resetTimer)
               withObject:nil
               afterDelay:4.f];
}

- (void)updateViewConstraints
{
    [super updateViewConstraints];
    
    NSArray *constraints = nil;
    NSLayoutConstraint *constraint = nil;
    
    if (IS_PAD) {
        NSDictionary *views = @{@"pageViewController" : self.pageViewController.view,
                                @"signinButton"       : self.signinButton,
                                @"signupButton"       : self.signupButton,
                                @"buttonBgView"       : self.buttonBgView,
                                @"pageControl"        : self.pageControl,
                                @"bgImageView"        : self.bgImageView,
                                @"blurView"           : self.blurView,
                                @"logoView"           : self.logoView};
        
        // bgImageView
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[bgImageView]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[bgImageView]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        // blureView
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[blurView]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.bgImageView addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[blurView]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.bgImageView addConstraints:constraints];
        
        // pageViewController
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[pageViewController]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[pageViewController]-%f-|", 80.f]
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        // buttonBgView
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[buttonBgView]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[buttonBgView(70)]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        // logoView
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-12-[logoView]-12-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraint = [NSLayoutConstraint constraintWithItem:self.logoView
                                                  attribute:NSLayoutAttributeWidth
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:nil
                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                 multiplier:1.f
                                                   constant:250.f];
        [self.logoView addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:self.logoView
                                                  attribute:NSLayoutAttributeLeading
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self.buttonBgView
                                                  attribute:NSLayoutAttributeLeading
                                                 multiplier:1.f
                                                   constant:0.f];
        [self.buttonBgView addConstraint:constraint];
        
        // Sign in/up button
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[signinButton(190)]-20-[signupButton(190)]-20-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[signinButton(40)]-15-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[signupButton(40)]-15-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraint = [NSLayoutConstraint constraintWithItem:self.signinButton
                                                  attribute:NSLayoutAttributeWidth
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self.signupButton
                                                  attribute:NSLayoutAttributeWidth
                                                 multiplier:1.f
                                                   constant:0];
        [self.view addConstraint:constraint];
        
        // pageControl
        constraint = [NSLayoutConstraint constraintWithItem:self.pageControl
                                                  attribute:NSLayoutAttributeCenterX
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self.view
                                                  attribute:NSLayoutAttributeCenterX
                                                 multiplier:1.f
                                                   constant:0.f];
        [self.view addConstraint:constraint];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[pageControl(10)]-10-[buttonBgView]"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
    } else {
        NSDictionary *views = @{@"pageViewController" : self.pageViewController.view,
                                @"signinButton"       : self.signinButton,
                                @"signupButton"       : self.signupButton,
                                @"buttonBgView"       : self.buttonBgView,
                                @"pageControl"        : self.pageControl,
                                @"bgImageView"        : self.bgImageView,
                                @"blurView"           : self.blurView};
        
        // bgImageView
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[bgImageView]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[bgImageView]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        // blurView
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[blurView]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[blurView]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        // pageViewController
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[pageViewController]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[pageViewController]-%f-|", 80.f]
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        // buttonBgView
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[buttonBgView]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[buttonBgView(70)]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        // Sign in/up button
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[signinButton]-20-[signupButton]-20-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[signinButton(40)]-15-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[signupButton(40)]-15-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraint = [NSLayoutConstraint constraintWithItem:self.signinButton
                                                  attribute:NSLayoutAttributeWidth
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self.signupButton
                                                  attribute:NSLayoutAttributeWidth
                                                 multiplier:1.f
                                                   constant:0];
        [self.view addConstraint:constraint];
        
        // pageControl
        constraint = [NSLayoutConstraint constraintWithItem:self.pageControl
                                                  attribute:NSLayoutAttributeCenterX
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self.view
                                                  attribute:NSLayoutAttributeCenterX
                                                 multiplier:1.f
                                                   constant:0.f];
        [self.view addConstraint:constraint];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[pageControl(10)]-10-[buttonBgView]"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];

    }
}

#pragma mark - Ping Example

- (void)_splash
{
    SKSplashIcon *pingSplashIcon = [[SKSplashIcon alloc] initWithImage:[UIImage imageNamed:@"logo.png"]
                                                         animationType:SKIconAnimationTypeFade];
    _splashView = [[SKSplashView alloc] initWithSplashIcon:pingSplashIcon
                                           backgroundColor:[UIColor alizarinColor]
                                             animationType:SKSplashAnimationTypeFade];
    _splashView.animationDuration = 1.5f;
    [self.view addSubview:_splashView];
    [_splashView startAnimation];
}

#pragma mark - getter

- (ISShimmerLogoView *)logoView
{
    if (! _logoView) {
        _logoView = [[ISShimmerLogoView alloc] init];
    }
    return _logoView;
}

- (UIVisualEffectView *)blurView
{
    if (! _blurView) {
        UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        _blurView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    }
    return _blurView;
}

- (UIImageView *)bgImageView
{
    if (! _bgImageView) {
        _bgImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"WelcomeBg1"]];
        _bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _bgImageView;
}

- (UIPageControl *)pageControl
{
    if (! _pageControl) {
        _pageControl = [[UIPageControl alloc] init];
        _pageControl.pageIndicatorTintColor        = [UIColor whiteColor];
        _pageControl.currentPageIndicatorTintColor = [UIColor alizarinColor];
        _pageControl.numberOfPages                 = [self numberOfPagesInPageViewController:self.pageViewController];
        _pageControl.currentPage                   = 0;
    }
    return _pageControl;
}

- (ISPageViewController *)pageViewController
{
    if (! _pageViewController) {
        _pageViewController = [[ISPageViewController alloc] init];
        _pageViewController.delegate = self;
        _pageViewController.dataSource = self;
    }
    return _pageViewController;
}

- (FUIButton *)signinButton
{
    if (! _signinButton) {
        _signinButton = [[FUIButton alloc] init];
        _signinButton.buttonColor = [UIColor whiteColor];
        _signinButton.shadowColor = [UIColor silverColor];
        _signinButton.shadowHeight = 2.f;
        _signinButton.cornerRadius = 6.f;
        _signinButton.titleLabel.font = [UIFont boldSystemFontOfSize:16.f];
        [_signinButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_signinButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        [_signinButton setTitle:NSLocalizedString(@"Sign In", @"ISWelcomeViewController") forState:UIControlStateNormal];
        _signinButton.accessibilityLabel = @"Sign In";
        [_signinButton addTarget:self
                          action:@selector(_signinButtonPressed:)
                forControlEvents:UIControlEventTouchUpInside];
    }
    return _signinButton;
}

- (FUIButton *)signupButton
{
    if (! _signupButton) {
        _signupButton = [[FUIButton alloc] init];
        _signupButton.buttonColor = [UIColor alizarinColor];
        _signupButton.shadowColor = [UIColor pomegranateColor];
        _signupButton.shadowHeight = 2.f;
        _signupButton.cornerRadius = 6.f;
        _signupButton.titleLabel.font = [UIFont boldSystemFontOfSize:16.f];
        [_signupButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_signupButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [_signupButton setTitle:NSLocalizedString(@"Sign Up", @"ISWelcomeViewController") forState:UIControlStateNormal];
        _signupButton.accessibilityLabel = @"Sign Up";
        [_signupButton addTarget:self
                          action:@selector(_signupButtonPressed:)
                forControlEvents:UIControlEventTouchUpInside];
    }
    return _signupButton;
}

- (UIView *)buttonBgView
{
    if (! _buttonBgView) {
        _buttonBgView = [[UIView alloc] init];
        [_buttonBgView setBackgroundColor:[[UIColor alloc] initWithWhite:1.f alpha:0.8f]];
    }
    return _buttonBgView;
}

#pragma mark - Selectors

- (void)_signinButtonPressed:(FUIButton *)sender
{
    ISSigninViewController *signinModal = [[ISSigninViewController alloc] init];
    ISNavigationController *nav = [[ISNavigationController alloc] initWithRootViewController:signinModal];
    MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:nav];
    formSheet.transitionStyle = MZFormSheetTransitionStyleBounce;
    formSheet.cornerRadius = 8.f;
    formSheet.shouldDismissOnBackgroundViewTap = YES;
    if (IS_PAD) {
        formSheet.presentedFormSheetSize = CGSizeMake(500, 290);
    } else {
        formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsMoveAboveKeyboard;
        formSheet.presentedFormSheetSize = CGSizeMake(CGRectGetWidth(self.view.frame)-20.f, 290);
    }
    
    [self mz_presentFormSheetController:formSheet
                               animated:YES
                      completionHandler:nil];
}


- (void)_signupButtonPressed:(FUIButton *)sender
{
    ISSignupViewController *signupModal = [[ISSignupViewController alloc] init];
    ISNavigationController *nav = [[ISNavigationController alloc] initWithRootViewController:signupModal];
    MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:nav];
    formSheet.transitionStyle = MZFormSheetTransitionStyleBounce;
    formSheet.cornerRadius = 8.f;
    formSheet.shouldDismissOnBackgroundViewTap = YES;
    if (IS_PAD) {
        formSheet.presentedFormSheetSize = CGSizeMake(500, 370);
    } else {
        formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsMoveAboveKeyboard;
        formSheet.presentedFormSheetSize = CGSizeMake(CGRectGetWidth(self.view.frame)-20.f, 370);
    }
    
    [self mz_presentFormSheetController:formSheet
                               animated:YES
                      completionHandler:nil];
}

#pragma mark - Page control

- (NSUInteger)pageIndex
{
    NSUInteger index = MIN(self.pageViewController.numberOfPages,
                           self.pageViewController.currentPage + 1);
    return index;
}

- (void)changePage
{
    NSUInteger index = [self pageIndex];
    if (index == [self numberOfPagesInPageViewController:self.pageViewController]) {
        [self.pageViewController navigateToPageAtIndex:0
                                              animated:NO
                                            completion:^{
                                                 [self hideBlur];
                                            }];
        [self.pageControl setCurrentPage:0];
    }
    
    [self.pageViewController navigateToPageAtIndex:index
                                          animated:YES
                                        completion:nil];
}

#pragma mark - PageView dataSource

- (NSUInteger)numberOfPagesInPageViewController:(ISPageViewController *)pageViewController
{
    return 6;
}

- (UIViewController *)pageViewController:(ISPageViewController *)pageViewController
            viewControllerForPageAtIndex:(NSUInteger)pageIndex
{
    // Here Prepare different viewController for each page
    if(self.viewControllerCache == nil) {
        self.viewControllerCache = [NSMutableDictionary dictionary];
    }
    
    ISPageChildViewController *pageChildViewController = self.viewControllerCache[@(pageIndex)];
    
    if(pageChildViewController == nil) {
        switch (pageIndex) {
            case 0:
            {
                pageChildViewController = [[ISPageChildViewController alloc] initWithImageName:@""
                                                                                          text:@""];
            }
                break;
                
            case 1:
            {
                pageChildViewController = [[ISPageChildViewController alloc] initWithImageName:@"page2"
                                                                                          text:@"Hello world! This is page 2!"];
            }
                break;
                
            case 2:
            {
                pageChildViewController = [[ISPageChildViewController alloc] initWithImageName:@"page3"
                                                                                          text:@"Hello world! This is page 3!"];
            }
                break;
            
            case 3:
            {
                pageChildViewController = [[ISPageChildViewController alloc] initWithImageName:@"page4"
                                                                                          text:@"Hello world! This is page 4!"];
            }
                break;
                
            case 4:
            {
                pageChildViewController = [[ISPageChildViewController alloc] initWithImageName:@"page5"
                                                                                          text:@"Hello world! This is page 5!"];
            }
                break;
                
            case 5:
            {
                pageChildViewController = [[ISPageChildViewController alloc] initWithImageName:@"page6"
                                                                                          text:@"Hello world! This is page 6!"];
            }
                break;
                
            default:
                break;
        }
        
        [pageChildViewController.view setFrame:self.pageViewController.view.bounds];
        self.viewControllerCache[@(pageIndex)] = pageChildViewController;
    }
    
    return pageChildViewController;
}

#pragma mark - ISPageViewControllerDelegate

- (void)pageViewController:(ISPageViewController *)pageViewController
     didShowViewController:(ISPageChildViewController *)controller
                   atIndex:(NSUInteger)index
{}

- (void)pageViewController:(ISPageViewController *)pageViewController
     didHideViewController:(UIViewController *)controller
                   atIndex:(NSUInteger)index
{}

- (void)pageViewController:(ISPageViewController *)pageViewController
       didNavigateToOffset:(CGPoint)offset
                   atIndex:(NSUInteger)index
{
    for (ISPageChildViewController *childViewController in pageViewController.visibleViewControllers) {
        [childViewController.view setAlpha:[self.pageViewController visiblePercentageForViewController:childViewController]];
    }
    if (index == 0) {
        [self hideBlur];
    } else {
        [self showBlur];
    }
    [self.pageControl setCurrentPage:index];
}

- (void)pageViewController:(ISPageViewController *)pageViewController
  didNavigateToPageAtIndex:(NSUInteger)pageIndex
{}

#pragma mark - Reset timer

- (void)resetTimer
{
    is_dispatch_main_async(^{
        if (self.timer) {
            [self.timer invalidate];
            _timer = nil;
        }
        
        self.timer = [NSTimer scheduledTimerWithTimeInterval:6.f
                                                      target:self
                                                    selector:@selector(changePage)
                                                    userInfo:nil
                                                     repeats:YES];
    })
}

- (void)showBlur
{
    if (self.blurView.hidden) {
        [UIView transitionWithView:self.blurView
                          duration:.2f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            [self.blurView setHidden:NO];
                        }
                        completion:nil];
    }
}

- (void)hideBlur
{
    if (! self.blurView.hidden) {
        [UIView transitionWithView:self.blurView
                          duration:.2f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            [self.blurView setHidden:YES];
                        }
                        completion:nil];
    }
}

@end
