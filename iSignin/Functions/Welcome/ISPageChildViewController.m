//
//  ISPageChildViewController.m
//  iSignin
//
//  Created by Damon Yuan on 29/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISPageChildViewController.h"

@interface ISPageChildViewController()

@property (nonatomic, strong) NSString *imageName;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, assign) NSString *text;
@property (nonatomic, strong) UILabel *label;

@end

@implementation ISPageChildViewController

- (instancetype)initWithImageName:(NSString *)imageName
                         text:(NSString *)text
{
    if (self = [self init]) {
        _imageName = imageName;
        _text = text;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setHidden:YES];
    [self _setupLayout];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.view setHidden:NO];
    [self.view setAlpha:0.f];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.view setHidden:YES];
}

- (void)_setupLayout
{
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:self.imageView];
    [self.view addSubview:self.label];
    
    [self.imageView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.label setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self.view setNeedsUpdateConstraints];
}

- (void)updateViewConstraints
{
    [super updateViewConstraints];
    
    NSDictionary *views = @{@"imageView" : self.imageView,
                            @"label" : self.label};
    
    NSArray *constraints;
    NSLayoutConstraint *constraint;
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[label]|"
                                                          options:0
                                                          metrics:nil
                                                            views:views];
    [self.view addConstraints:constraints];
    
    CGRect windowFrame = [[UIScreen mainScreen] bounds];
    
    constraint = [NSLayoutConstraint constraintWithItem:self.imageView
                                              attribute:NSLayoutAttributeWidth
                                              relatedBy:NSLayoutRelationEqual
                                                 toItem:nil
                                              attribute:NSLayoutAttributeNotAnAttribute
                                             multiplier:1.f
                                               constant:windowFrame.size.width - 40.f];
    [self.view addConstraint:constraint];
    
    constraint = [NSLayoutConstraint constraintWithItem:self.imageView
                                              attribute:NSLayoutAttributeCenterX
                                              relatedBy:NSLayoutRelationEqual
                                                 toItem:self.label
                                              attribute:NSLayoutAttributeCenterX
                                             multiplier:1.f
                                               constant:0.f];
    [self.view addConstraint:constraint];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[imageView][label(30)]-30-|"
                                                          options:0
                                                          metrics:nil
                                                            views:views];
    [self.view addConstraints:constraints];
    
    constraint = [NSLayoutConstraint constraintWithItem:self.imageView
                                              attribute:NSLayoutAttributeHeight
                                              relatedBy:NSLayoutRelationEqual
                                                 toItem:nil
                                              attribute:NSLayoutAttributeNotAnAttribute
                                             multiplier:1.f
                                               constant:windowFrame.size.height - 180.f];
    [self.view addConstraint:constraint];
}

#pragma mark - Getter

- (UIImageView *)imageView {
    if (! _imageView) {
        _imageView = [[UIImageView alloc] init];
        if (! [NSString isNilOrEmpty:self.imageName]) {
            [_imageView setImage:[UIImage imageNamed:self.imageName]];
        }
        [_imageView setContentMode:UIViewContentModeScaleAspectFit];
    }
    return _imageView;
}

- (UILabel *)label
{
    if (! _label) {
        _label = [[UILabel alloc] init];
        [_label setText:self.text];
        [_label setBackgroundColor:[UIColor clearColor]];
        [_label setTextAlignment:NSTextAlignmentCenter];
        [_label setTextColor:[UIColor alizarinColor]];
        [_label setFont:[UIFont italicFlatFontOfSize:20.f]];
    }
    return _label;
}

@end
