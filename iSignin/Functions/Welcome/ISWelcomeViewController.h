//
//  ISWelcomeViewController.h
//  iSignin
//
//  Created by Damon Yuan on 22/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface ISWelcomeViewController : UIViewController

- (instancetype)initWithSplash:(BOOL)isSplash;

@end
