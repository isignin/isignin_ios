//
//  ISPageViewController.h
//  iSignin
//
//  Created by Damon Yuan on 28/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ISPageViewControllerDataSource;
@protocol ISPageViewControllerDelegate;
@protocol ISPageLayouterProtocol;
@protocol ISEasingFunctionProtocol;

@interface ISPageViewController : UIViewController

- (void)reloadData;
- (void)reloadPageAtIndex:(NSUInteger)index;
- (void)navigateToPageAtIndex:(NSUInteger)pageIndex
                     animated:(BOOL)animated
                   completion:(void(^)())completion;
- (CGFloat)visiblePercentageForViewController:(UIViewController *)viewController;
- (void)setLayouter:(id<ISPageLayouterProtocol>)layouter
           animated:(BOOL)animated
         completion:(void(^)())completion;

@property (nonatomic, readonly) id<ISPageLayouterProtocol> layouter;
@property (nonatomic, weak) id<ISPageViewControllerDataSource> dataSource;
@property (nonatomic, weak) id<ISPageViewControllerDelegate> delegate;
@property (nonatomic, readonly) NSUInteger currentPage;
@property (nonatomic, readonly) NSUInteger numberOfPages;
@property (nonatomic, readonly) NSArray *loadedViewControllers;
@property (nonatomic, readonly) NSArray *visibleViewControllers;
@property (nonatomic, strong) UIBezierPath *touchRefusalArea;
@property (nonatomic, assign) BOOL bounces;
@property (nonatomic, assign) BOOL scrollEnabled;
@property (nonatomic, assign) BOOL showsScrollIndicators;
@property (nonatomic, assign) BOOL pagingEnabled;
@property (nonatomic, assign) BOOL continuousNavigationEnabled;
@property (nonatomic, assign) NSUInteger minimumNumberOfTouches;
@property (nonatomic, assign) NSUInteger maximumNumberOfTouches;
@property (nonatomic, strong) id<ISEasingFunctionProtocol> easingFunction;
@property (nonatomic, assign) NSTimeInterval animationDuration;
@property (nonatomic, assign) CGFloat decelerationRate;
@property (nonatomic, assign) BOOL fadeEnabled;

@end

@protocol ISPageViewControllerDataSource <NSObject>

- (NSUInteger)numberOfPagesInPageViewController:(ISPageViewController *)pageViewController;
- (UIViewController *)pageViewController:(ISPageViewController *)pageViewController
            viewControllerForPageAtIndex:(NSUInteger)pageIndex;

@end

@protocol ISPageViewControllerDelegate <NSObject>

@optional

- (void)pageViewController:(ISPageViewController *)pageViewController
     didShowViewController:(UIViewController *)controller
                   atIndex:(NSUInteger)index;

- (void)pageViewController:(ISPageViewController *)pageViewController
     didHideViewController:(UIViewController *)controller
                   atIndex:(NSUInteger)index;

- (void)pageViewController:(ISPageViewController *)pageViewController
       didNavigateToOffset:(CGPoint)offset
                   atIndex:(NSUInteger)index;

- (void)pageViewController:(ISPageViewController *)pageViewController
  didNavigateToPageAtIndex:(NSUInteger)pageIndex;


@end