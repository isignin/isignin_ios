//
//  ISPageViewControllerView.h
//  iSignin
//
//  Created by Damon Yuan on 29/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ISPageViewControllerViewDelegate;

@interface ISPageViewControllerView : UIView

@property (nonatomic, weak) id<ISPageViewControllerViewDelegate> delegate;

@end

@protocol ISPageViewControllerViewDelegate <NSObject>

- (void)pageViewControllerViewWillChangeFrame:(ISPageViewControllerView *)pageViewControllerView;
- (void)pageViewControllerViewDidChangeFrame:(ISPageViewControllerView *)pageViewControllerView;

@end
