//
//  ISPageViewControllerView.m
//  iSignin
//
//  Created by Damon Yuan on 29/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISPageViewControllerView.h"

@implementation ISPageViewControllerView

- (void)setFrame:(CGRect)frame
{
    [self.delegate pageViewControllerViewWillChangeFrame:self];
    super.frame = frame;
    [self.delegate pageViewControllerViewDidChangeFrame:self];
}

@end
