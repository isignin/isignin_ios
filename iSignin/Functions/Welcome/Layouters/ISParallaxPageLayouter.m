//
//  ISParallaxPageLayouter.m
//  iSignin
//
//  Created by Damon Yuan on 29/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISParallaxPageLayouter.h"

@implementation ISParallaxPageLayouter

- (id)init
{
    if (self = [super init]) {
        self.interItemSpacing = 0.f;
    }
    return self;
}

- (CGRect)currentFrameForViewController:(UIViewController *)viewController
                              withIndex:(NSUInteger)index
                          contentOffset:(CGPoint)contentOffset
                             finalFrame:(CGRect)finalFrame
                   inPageViewController:(ISPageViewController *)pageViewController
{
    if (index == 0) {
        return finalFrame;
    }
    
    if (self.navigationType == ISPageLayouterNavigationTypeVertical) {
        CGFloat ratio = 1.f - (CGRectGetMinY(finalFrame) - contentOffset.y) / (CGRectGetHeight(finalFrame) + CGRectGetHeight(finalFrame) / 2);
        finalFrame.origin.y = (CGRectGetMinY(finalFrame) - CGRectGetHeight(finalFrame)) + CGRectGetHeight(finalFrame) * MAX(0.f, MIN(1.f, ratio));
    } else {
        CGFloat ratio = 1.f - (CGRectGetMinX(finalFrame) - contentOffset.x) / (CGRectGetWidth(finalFrame) + CGRectGetWidth(finalFrame) / 2);
        finalFrame.origin.x = (CGRectGetMinX(finalFrame) - CGRectGetWidth(finalFrame)) + CGRectGetWidth(finalFrame) * MAX(0.f, MIN(1.f, ratio));
    }
    
    return finalFrame;
}

@end
