//
//  ISSlidingPageLayouter.m
//  iSignin
//
//  Created by Damon Yuan on 29/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISSlidingPageLayouter.h"

@implementation ISSlidingPageLayouter

- (id)init
{
    if(self = [super init]) {
        self.interItemSpacing = 100.0f;
    }
    
    return self;
}

- (CGRect)currentFrameForViewController:(UIViewController *)viewController
                              withIndex:(NSUInteger)index
                          contentOffset:(CGPoint)contentOffset
                             finalFrame:(CGRect)finalFrame
                   inPageViewController:(ISPageViewController *)pageViewController
{
    if(index == 0) {
        return finalFrame;
    }
    
    if(self.navigationType == ISPageLayouterNavigationTypeVertical) {
        finalFrame.origin.y = MAX(finalFrame.origin.y - finalFrame.size.height, MIN(CGRectGetMaxY(finalFrame) - CGRectGetHeight(finalFrame), contentOffset.y));
    } else {
        finalFrame.origin.x = MAX(finalFrame.origin.x - finalFrame.size.width, MIN(CGRectGetMaxX(finalFrame) - CGRectGetWidth(finalFrame), contentOffset.x));
    }
    
    return finalFrame;
}

@end
