//
//  ISPaperPageLayouter.h
//  iSignin
//
//  Created by Damon Yuan on 29/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISPageLayouter.h"

@interface ISPaperPageLayouter : NSObject <ISPageLayouterProtocol>

@end
