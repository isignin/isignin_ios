//
//  ISPageLayouter.m
//  iSignin
//
//  Created by Damon Yuan on 29/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISPageLayouter.h"

@implementation ISPageLayouter
@synthesize navigationType;
@synthesize interItemSpacing;
@synthesize numberOfPagesToPreloadBeforeCurrentPage;
@synthesize numberOfPagesToPreloadAfterCurrentPage;
@synthesize contentInsets;
@synthesize navigationConstraintType;

- (id)init
{
    if (self = [super init]) {
        self.interItemSpacing = 50.f;
        self.numberOfPagesToPreloadBeforeCurrentPage = 1;
        self.numberOfPagesToPreloadAfterCurrentPage  = 1;
        self.navigationConstraintType = ISPageLayouterNavigationConstraintTypeForward | ISPageLayouterNavigationConstraintTypeReverse;
    }
    return self;
}

- (CGRect)finalFrameForPageAtIndex:(NSUInteger)index
              inPageViewController:(ISPageViewController *)pageViewController
{
    // Here to set the frame for each page
    CGRect frame = pageViewController.view.bounds;
    if (self.navigationType == ISPageLayouterNavigationTypeVertical) {
        frame.origin.y = index * (CGRectGetHeight(frame) + self.interItemSpacing);
    } else {
        frame.origin.x = index * (CGRectGetWidth(frame) + self.interItemSpacing);
    }
    return frame;
}

- (CGRect)currentFrameForViewController:(UIViewController *)viewController
                              withIndex:(NSUInteger)index
                          contentOffset:(CGPoint)contentOffset
                             finalFrame:(CGRect)finalFrame
                   inPageViewController:(ISPageViewController *)pageViewController
{
    return finalFrame;
}

@end
