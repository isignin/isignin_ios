//
//  ISPageLayouterProtocol.h
//  iSignin
//
//  Created by Damon Yuan on 29/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISPageViewController.h"

typedef enum {
    ISPageLayouterNavigationTypeHorizontal,
    ISPageLayouterNavigationTypeVertical
} ISPageLayouterNavigationType;

typedef enum {
    ISPageLayouterNavigationConstraintTypeNone = 0,
    ISPageLayouterNavigationConstraintTypeForward = 1 << 0,
    ISPageLayouterNavigationConstraintTypeReverse = 1 << 1
} ISPageLayouterNavigationConstraintType;

@protocol ISPageLayouterProtocol <NSObject>

@property (nonatomic, assign) ISPageLayouterNavigationType navigationType;
@property (nonatomic, assign) ISPageLayouterNavigationConstraintType navigationConstraintType;
@property (nonatomic, assign) CGFloat interItemSpacing;
@property (nonatomic, assign) UIEdgeInsets contentInsets;
@property (nonatomic, assign) NSUInteger numberOfPagesToPreloadBeforeCurrentPage;
@property (nonatomic, assign) NSUInteger numberOfPagesToPreloadAfterCurrentPage;

- (CGRect)finalFrameForPageAtIndex:(NSUInteger)index
              inPageViewController:(ISPageViewController *)pageViewController;

- (CGRect)currentFrameForViewController:(UIViewController *)viewController
                              withIndex:(NSUInteger)index
                          contentOffset:(CGPoint)contentOffset
                             finalFrame:(CGRect)finalFrame
                   inPageViewController:(ISPageViewController *)pageViewController;

@optional

- (void)pageViewController:(ISPageViewController *)pageViewController
       didNavigateToOffset:(CGPoint)offset;

@end
