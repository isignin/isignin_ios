//
//  ISPaperPageLayouter.m
//  iSignin
//
//  Created by Damon Yuan on 29/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISPaperPageLayouter.h"

static const CGFloat horizontalInset = 192.f;
static const CGFloat verticalInset = 256.f;

@implementation ISPaperPageLayouter
@synthesize interItemSpacing;
@synthesize navigationType;
@synthesize numberOfPagesToPreloadAfterCurrentPage;
@synthesize numberOfPagesToPreloadBeforeCurrentPage;
@synthesize contentInsets;
@synthesize navigationConstraintType;

- (id)init
{
    if (self = [super init]) {
        self.interItemSpacing = 20.f;
        self.numberOfPagesToPreloadBeforeCurrentPage = 2;
        self.numberOfPagesToPreloadAfterCurrentPage  = 2;
        self.contentInsets = UIEdgeInsetsMake(0.f, 0.f, 0.f, 384);
        self.navigationConstraintType = ISPageLayouterNavigationConstraintTypeForward | ISPageLayouterNavigationConstraintTypeReverse;
    }
    return self;
}

- (CGRect)finalFrameForPageAtIndex:(NSUInteger)index
              inPageViewController:(ISPageViewController *)pageViewController
{
    CGRect frame = CGRectInset(pageViewController.view.bounds, horizontalInset, verticalInset);
    frame.origin = CGPointZero;
    
    if(self.navigationType == ISPageLayouterNavigationTypeVertical) {
        frame.origin.y = index * (CGRectGetHeight(frame) + self.interItemSpacing);
        frame.origin.x = CGRectGetWidth(pageViewController.view.bounds) - CGRectGetWidth(frame);
    } else {
        frame.origin.x = index * (CGRectGetWidth(frame) + self.interItemSpacing);
        frame.origin.y = CGRectGetHeight(pageViewController.view.bounds) - CGRectGetHeight(frame);
    }
    
    return frame;
}

- (CGRect)currentFrameForViewController:(UIViewController *)viewController
                              withIndex:(NSUInteger)index
                          contentOffset:(CGPoint)contentOffset
                             finalFrame:(CGRect)finalFrame
                   inPageViewController:(ISPageViewController *)pageViewController;
{
    return finalFrame;
}




@end
