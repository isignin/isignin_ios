//
//  ISPageChildViewController.h
//  iSignin
//
//  Created by Damon Yuan on 29/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISPageChildViewController : UIViewController

- (instancetype)initWithImageName:(NSString *)imageName
                             text:(NSString *)text;

@end
