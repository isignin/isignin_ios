//
//  ISRightMenuViewController.h
//  iSignin
//
//  Created by Damon Yuan on 13/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

#define kISRightMenuItemHeight 54.f

typedef NS_ENUM(NSUInteger, ISRightMenuItem) {
    kISRightMenuItemHome = 0,
    kISRightMenuItemProfile,
    kISRightMenuItemSettings,
    kISRightMenuItemLogout,
    kISRightMenuItemCount
};

@interface ISLeftMenuViewController : UIViewController
<
    UITableViewDataSource,
    UITableViewDelegate,
    RESideMenuDelegate
>

@end
