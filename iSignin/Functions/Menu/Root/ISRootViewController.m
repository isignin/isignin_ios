//
//  ISRootViewController.m
//  iSignin
//
//  Created by Damon Yuan on 13/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISRootViewController.h"

@interface ISRootViewController ()

@end

@implementation ISRootViewController

- (void)awakeFromNib
{
    self.menuPreferredStatusBarStyle = UIStatusBarStyleLightContent;
    self.contentViewShadowColor      = [UIColor blackColor];
    self.contentViewShadowOffset     = CGSizeMake(0, 0);
    self.contentViewShadowOpacity    = 0.6;
    self.contentViewShadowRadius     = 12;
    self.contentViewShadowEnabled    = YES;
    self.contentViewScaleValue       = 0.9;
    self.contentViewInLandscapeOffsetCenterX = -280.f;
    
    self.contentViewController   = [self.storyboard instantiateViewControllerWithIdentifier:@"contentViewController"];
    self.leftMenuViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"leftMenuViewController"];
    self.backgroundImage         = [UIImage imageNamed:@"Stars"];
    self.delegate                = self;
}

#pragma mark -
#pragma mark RESideMenu Delegate

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController
{
    ISLogDebug(@"willShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController
{
    ISLogDebug(@"didShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController
{
    ISLogDebug(@"willHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController
{
    ISLogDebug(@"didHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}


@end
