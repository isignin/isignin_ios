//
//  ISRootViewController.h
//  iSignin
//
//  Created by Damon Yuan on 13/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@interface ISRootViewController : RESideMenu <RESideMenuDelegate>

@end
