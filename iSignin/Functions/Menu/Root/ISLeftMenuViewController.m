//
//  ISRightMenuViewController.m
//  iSignin
//
//  Created by Damon Yuan on 13/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISLeftMenuViewController.h"

@interface ISLeftMenuViewController ()

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation ISLeftMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,
                                                                           (CGRectGetHeight(self.view.frame) - kISRightMenuItemHeight * kISRightMenuItemCount) * 0.5,
                                                                           CGRectGetWidth(self.view.frame),
                                                                           kISRightMenuItemHeight * kISRightMenuItemCount)
                                                          style:UITableViewStylePlain];
    tableView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
    tableView.delegate         = self;
    tableView.dataSource       = self;
    tableView.opaque           = NO;
    tableView.backgroundColor  = [UIColor clearColor];
    tableView.backgroundView   = nil;
    tableView.separatorStyle   = UITableViewCellSeparatorStyleNone;
    tableView.bounces          = NO;
    tableView.scrollsToTop     = NO;
    
    self.tableView = tableView;
    self.tableView.accessibilityIdentifier = @"Menu List";
    self.tableView.isAccessibilityElement = YES;
    [self.view addSubview:self.tableView];
}

#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"homeViewController"]]
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            break;
        case 1:
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"profileViewController"]]
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            break;
        case 2:
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"settingsViewController"]]
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            break;
        case 3:
            [ISNavigator logout];
            break;

        default:
            break;
    }
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kISRightMenuItemHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:21];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.highlightedTextColor = [UIColor lightGrayColor];
        cell.selectedBackgroundView = [[UIView alloc] init];
    }
    
    NSArray *titles = @[NSLocalizedString(@"Home", @"ISLeftMenuViewController"),
                        NSLocalizedString(@"Profile", @"ISLeftMenuViewController"),
                        NSLocalizedString(@"Settings", @"ISLeftMenuViewController"),
                        NSLocalizedString(@"Log Out", @"ISLeftMenuViewController")];
    NSArray *images = @[@"IconHome",
                        @"IconProfile",
                        @"IconSettings",
                        @"IconLogout"];
    cell.textLabel.text = titles[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:images[indexPath.row]];
    
    return cell;
}


@end
