//
//  ISSettingsViewController.m
//  iSignin
//
//  Created by Damon Yuan on 13/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISSettingsViewController.h"

@interface ISSettingsViewController ()

@end

@implementation ISSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.leftBarButtonItem.accessibilityLabel = @"Left Nav Button";
}


@end
