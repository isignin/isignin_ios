//
//  ISForgetPasswordViewController.m
//  iSignin
//
//  Created by Damon Yuan on 23/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISForgetPasswordViewController.h"
#import "ISGatewayClient.h"
#import "RegexKitLite.h"
#import "MZFormSheetController.h"
#import "AppDelegate.h"

@interface ISForgetPasswordViewController()

@property (nonatomic, strong) UIImageView  *emailImageView;
@property (nonatomic, strong) UILabel      *explanationLabel;
@property (nonatomic, strong) FUITextField *emailTextField;
@property (nonatomic, strong) FUIButton    *resetPasswordButton;
@property (nonatomic, strong) MBProgressHUD *hud;

@end

@implementation ISForgetPasswordViewController

- (void)dealloc
{
    _emailTextField.delegate = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self _setupLayout];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.emailTextField becomeFirstResponder];
}

- (void)_setupLayout
{
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.emailImageView];
    [self.view addSubview:self.explanationLabel];
    [self.view addSubview:self.emailTextField];
    [self.view addSubview:self.resetPasswordButton];
    
    self.emailImageView.translatesAutoresizingMaskIntoConstraints      = NO;
    self.explanationLabel.translatesAutoresizingMaskIntoConstraints    = NO;
    self.emailTextField.translatesAutoresizingMaskIntoConstraints      = NO;
    self.resetPasswordButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view setNeedsUpdateConstraints];
}

- (void)updateViewConstraints
{
    [super updateViewConstraints];
    
    NSDictionary *views = @{@"emailImageView"      : self.emailImageView,
                            @"explanationLabel"    : self.explanationLabel,
                            @"emailTextField"      : self.emailTextField,
                            @"resetPasswordButton" : self.resetPasswordButton};
    NSArray *constraints = nil;
    NSLayoutConstraint *constraint = nil;
    
    if (IS_PAD) {
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[emailImageView(60)]"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraint = [NSLayoutConstraint constraintWithItem:self.emailImageView
                                                  attribute:NSLayoutAttributeCenterX
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self.view
                                                  attribute:NSLayoutAttributeCenterX
                                                 multiplier:1.f
                                                   constant:0.f];
        [self.view addConstraint:constraint];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-60-[explanationLabel]-60-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-60-[emailTextField]-60-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-60-[resetPasswordButton]-60-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[emailImageView(60)]-10-[explanationLabel(60)]-0-[emailTextField(40)]-10-[resetPasswordButton(40)]-10-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
    } else {
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[emailImageView(60)]"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraint = [NSLayoutConstraint constraintWithItem:self.emailImageView
                                                  attribute:NSLayoutAttributeCenterX
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self.view
                                                  attribute:NSLayoutAttributeCenterX
                                                 multiplier:1.f
                                                   constant:0.f];
        [self.view addConstraint:constraint];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[explanationLabel]-20-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[emailTextField]-20-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[resetPasswordButton]-20-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[emailImageView(60)]-10-[explanationLabel(60)]-0-[emailTextField(40)]-10-[resetPasswordButton(40)]-10-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
    }
}

#pragma mark - Getter

- (FUITextField *)emailTextField
{
    if (! _emailTextField) {
        _emailTextField                    = [[FUITextField alloc] init];
        _emailTextField.delegate           = self;
        _emailTextField.keyboardType       = UIKeyboardTypeEmailAddress;
        _emailTextField.font               = [UIFont flatFontOfSize:16];
        _emailTextField.textFieldColor     = [UIColor cloudsColor];
        _emailTextField.backgroundColor    = [ISAppStyleSheet defaultBackgroundColor];
        _emailTextField.edgeInsets         = UIEdgeInsetsMake(4.f,15.f, 4.f, 15.f);
        _emailTextField.borderColor        = [UIColor turquoiseColor];
        _emailTextField.borderWidth        = 2.f;
        _emailTextField.cornerRadius       = 3.f;
        _emailTextField.placeholder        = NSLocalizedString(@"Email", @"ISForgetPasswordViewController");
        _emailTextField.accessibilityLabel = @"Email text field";
    }
    return _emailTextField;
}

- (UIImageView *)emailImageView
{
    if (! _emailImageView) {
        _emailImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Mail"]];
        _emailImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _emailImageView;
}

- (UILabel *)explanationLabel
{
    if (! _explanationLabel) {
        _explanationLabel = [[UILabel alloc] init];
        _explanationLabel.font = [UIFont flatFontOfSize:16.f];
        _explanationLabel.textColor = [UIColor turquoiseColor];
        _explanationLabel.backgroundColor = [UIColor whiteColor];
        _explanationLabel.numberOfLines = 1;
        _explanationLabel.textAlignment = NSTextAlignmentCenter;
        [_explanationLabel setText:NSLocalizedString(@"Retrieve your password through email.", @"ISForgetPasswordViewController")];
    }
    return _explanationLabel;
}

- (FUIButton *)resetPasswordButton
{
    if (! _resetPasswordButton) {
        _resetPasswordButton = [[FUIButton alloc] init];
        _resetPasswordButton.buttonColor  = [UIColor turquoiseColor];
        _resetPasswordButton.shadowColor  = [UIColor greenSeaColor];
        _resetPasswordButton.shadowHeight = 2.f;
        _resetPasswordButton.cornerRadius = 6.f;
        _resetPasswordButton.titleLabel.font = [UIFont boldSystemFontOfSize:16.f];
        [_resetPasswordButton setTitleColor:[UIColor whiteColor]
                                   forState:UIControlStateNormal];
        [_resetPasswordButton setTitleColor:[UIColor whiteColor]
                                   forState:UIControlStateHighlighted];
        [_resetPasswordButton setTitle:NSLocalizedString(@"Reset password", @"ISForgetPasswordViewController")
                              forState:UIControlStateNormal];
        _resetPasswordButton.accessibilityLabel = @"reset password button";
        [_resetPasswordButton addTarget:self
                                 action:@selector(_resetPasswordButtonPressed:)
                       forControlEvents:UIControlEventTouchUpInside];
    }
    return _resetPasswordButton;
}

- (MBProgressHUD *)hud
{
    if (! _hud) {
        _hud = [[MBProgressHUD alloc] initWithView:self.view];
        _hud.userInteractionEnabled = NO;
        [self.view addSubview:_hud];
    }
    return _hud;
}

#pragma mark - Selector methods

- (void)_resetPasswordButtonPressed:(FUIButton *)sender
{
    [self.view endEditing:YES];
    if ([self _isValid])
    {
        [self.hud show:YES];
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            ISGatewayClient *client = [ISGatewayClient sharedClient];
            NSDictionary *params = @{@"user" : @{@"email" : self.emailTextField.text}};
            ISHTTPClientSuccessBlock success = ^(NSURLSessionDataTask *task, id responseObject)
            {
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                [self.hud hide:YES];
                [ISAppStyleSheet showAlertWithMessage:NSLocalizedString(@"An reset email has been forward to your address.", @"ISForgetPasswordViewController")];
            };
            
            ISHTTPClientFailureBlock failure = ^(NSURLSessionDataTask *task, NSError *error)
            {
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                [self.hud hide:YES];
                [ISAppStyleSheet showAlertWithMessage:[error localizedDescription]];
            };
            
            [client POST:@"users/password"
              parameters:params
                 success:success
                 failure:failure];
        });
    }
}

- (BOOL)_isValid
{
    if ([NSString isNilOrEmpty:self.emailTextField.text]) {
        [ISAppStyleSheet showAlertWithMessage:NSLocalizedString(@"Please enter your Email.", @"ISSignupViewController")];
        return NO;
    } else if (! [self.emailTextField.text isMatchedByRegex:IS_EMAIL_REGEX]) {
        [ISAppStyleSheet showAlertWithMessage:NSLocalizedString(@"Please enter valid Email.", @"ISSignupViewController")];
        return NO;
    }
    return YES;
}



@end
