//
//  ISSigninViewController.m
//  iSignin
//
//  Created by Damon Yuan on 22/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISSigninViewController.h"
#import "ISGatewayClient.h"
#import "ISForgetPasswordViewController.h"
#import "SSKeychain.h"
#import "MZFormSheetController.h"
#import "AppDelegate.h"
#import "RegexKitLite.h"

@interface ISSigninViewController ()

@property (nonatomic, strong) FUITextField *emailTextField;
@property (nonatomic, strong) FUITextField *passwordTextField;
@property (nonatomic, strong) FUIButton    *signinButton;
@property (nonatomic, strong) UIButton     *forgotPasswordButton;
@property (nonatomic, strong) MBProgressHUD *hud;

@end

@implementation ISSigninViewController

- (void)dealloc
{
    _emailTextField.delegate = nil;
    _passwordTextField.delegate  = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self _setupNavigationbar];
    [self _setupLayout];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.emailTextField becomeFirstResponder];
}

- (void)_setupNavigationbar
{
    self.title = NSLocalizedString(@"Sign In", @"ISSigninViewController");
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                  target:self
                                                                                  action:@selector(_cancelButtonPressed:)];
    cancelButton.accessibilityLabel = @"cancel sign in button";
    self.navigationItem.leftBarButtonItem = cancelButton;
}

- (void)_setupLayout
{
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [self.view addSubview:self.emailTextField];
    [self.view addSubview:self.passwordTextField];
    [self.view addSubview:self.signinButton];
    [self.view addSubview:self.forgotPasswordButton];
    
    self.emailTextField.translatesAutoresizingMaskIntoConstraints   = NO;
    self.passwordTextField.translatesAutoresizingMaskIntoConstraints    = NO;
    self.signinButton.translatesAutoresizingMaskIntoConstraints         = NO;
    self.forgotPasswordButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view setNeedsUpdateConstraints];
}

- (void)updateViewConstraints
{
    [super updateViewConstraints];
    
    NSDictionary *views = @{@"emailNameTextField"   : self.emailTextField,
                            @"passwordTextField"    : self.passwordTextField,
                            @"signinButton"         : self.signinButton,
                            @"forgotPasswordButton" : self.forgotPasswordButton};
    NSArray *constraints = nil;
    
    if (IS_PAD) {
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-60-[emailNameTextField]-60-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-60-[passwordTextField]-60-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-60-[signinButton]-60-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-60-[forgotPasswordButton(100)]"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[emailNameTextField(40)]-20-[passwordTextField(40)]-20-[signinButton(40)]-20-[forgotPasswordButton(20)]-20-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
    } else {
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[emailNameTextField]-20-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[passwordTextField]-20-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[signinButton]-20-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[forgotPasswordButton(100)]"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[emailNameTextField(40)]-20-[passwordTextField(40)]-20-[signinButton(40)]-20-[forgotPasswordButton(20)]-20-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];

    }
}

#pragma mark - getter

- (FUITextField *)emailTextField
{
    if (! _emailTextField) {
        _emailTextField                    = [[FUITextField alloc] init];
        _emailTextField.delegate           = self;
        _emailTextField.keyboardType       = UIKeyboardTypeEmailAddress;
        _emailTextField.font               = [UIFont flatFontOfSize:16];
        _emailTextField.textFieldColor     = [UIColor cloudsColor];
        _emailTextField.backgroundColor    = [ISAppStyleSheet defaultBackgroundColor];
        _emailTextField.edgeInsets         = UIEdgeInsetsMake(4.f,15.f, 4.f, 15.f);
        _emailTextField.borderColor        = [UIColor turquoiseColor];
        _emailTextField.borderWidth        = 2.f;
        _emailTextField.cornerRadius       = 3.f;
        _emailTextField.placeholder        = NSLocalizedString(@"Email", @"ISSigninViewController");
        _emailTextField.accessibilityLabel = @"Email Username text field";
    }
    return _emailTextField;
}

- (FUITextField *)passwordTextField
{
    if (! _passwordTextField) {
        _passwordTextField                    = [[FUITextField alloc] init];
        _passwordTextField.delegate           = self;
        _passwordTextField.secureTextEntry    = YES;
        _passwordTextField.font               = [UIFont flatFontOfSize:16];
        _passwordTextField.textFieldColor     = [UIColor cloudsColor];
        _passwordTextField.backgroundColor    = [ISAppStyleSheet defaultBackgroundColor];
        _passwordTextField.edgeInsets         = UIEdgeInsetsMake(4.f,15.f, 4.f, 15.f);
        _passwordTextField.borderColor        = [UIColor turquoiseColor];
        _passwordTextField.borderWidth        = 2.f;
        _passwordTextField.cornerRadius       = 3.f;
        _passwordTextField.placeholder        = NSLocalizedString(@"Password (at least 6 characters)", @"ISSigninViewController");
        _passwordTextField.accessibilityLabel = @"Password text field";
    }
    return _passwordTextField;
}

- (FUIButton *)signinButton
{
    if (! _signinButton) {
        _signinButton = [[FUIButton alloc] init];
        _signinButton.buttonColor  = [UIColor turquoiseColor];
        _signinButton.shadowColor  = [UIColor greenSeaColor];
        _signinButton.shadowHeight = 2.f;
        _signinButton.cornerRadius = 6.f;
        _signinButton.titleLabel.font = [UIFont boldSystemFontOfSize:16.f];
        [_signinButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_signinButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [_signinButton setTitle:NSLocalizedString(@"Sign In", @"ISSigninViewController") forState:UIControlStateNormal];
        _signinButton.accessibilityLabel = @"Sign in button";
        [_signinButton addTarget:self
                          action:@selector(_signinButtonPressed:)
                forControlEvents:UIControlEventTouchUpInside];
    }
    return _signinButton;
}

- (UIButton *)forgotPasswordButton
{
    if (! _forgotPasswordButton) {
        _forgotPasswordButton = [[UIButton alloc] init];
        _forgotPasswordButton.titleLabel.font = [UIFont italicFlatFontOfSize:10.f];
        _forgotPasswordButton.layer.cornerRadius = 10.f;
        _forgotPasswordButton.layer.borderColor = [[UIColor turquoiseColor] CGColor];
        _forgotPasswordButton.layer.borderWidth = 1.f;
        [_forgotPasswordButton setBackgroundColor:[UIColor whiteColor]];
        [_forgotPasswordButton setTitleColor:[UIColor blackColor]
                                    forState:UIControlStateNormal];
        [_forgotPasswordButton setTitle:NSLocalizedString(@"Forget password?", @"ISSigninViewController")
                               forState:UIControlStateNormal];
        _forgotPasswordButton.accessibilityLabel = @"Forget password button";
        [_forgotPasswordButton addTarget:self
                                  action:@selector(_forgetPasswordButtonPressed:)
                        forControlEvents:UIControlEventTouchUpInside];
    }
    return _forgotPasswordButton;
}

- (MBProgressHUD *)hud
{
    if (! _hud) {
        _hud = [[MBProgressHUD alloc] initWithView:self.view];
        _hud.userInteractionEnabled = NO;
        [self.view addSubview:_hud];
    }
    return _hud;
}

#pragma mark - Selector methods

- (void)_signinButtonPressed:(id)sender
{
    [self.view endEditing:YES];
    if ([self _isValid])
    {
        [self.hud show:YES];
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            ISGatewayClient *client = [ISGatewayClient sharedClient];

            NSDictionary *params = @{@"user" : @{@"email"    : self.emailTextField.text,
                                                 @"password" : self.passwordTextField.text}};
            __weak __typeof__(self) weakSelf = self;
            ISHTTPClientSuccessBlock success = ^(NSURLSessionDataTask *task, id responseObject)
            {
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                [self.hud hide:YES];
                NSString *token = responseObject[@"user"][@"api_token"];
                if ([token isKindOfClass:[NSString class]] &&
                    ! [NSString isNilOrEmpty:token])
                {
                    __typeof__(self) strongSelf = weakSelf;
                    [strongSelf mz_dismissFormSheetControllerAnimated:NO
                                                    completionHandler:nil];
                    [ISNavigator loginWithAccount:self.emailTextField.text
                                            token:token];
                }
            };
            
            ISHTTPClientFailureBlock failure = ^(NSURLSessionDataTask *task, NSError *error)
            {
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                [self.hud hide:YES];
                [ISAppStyleSheet showAlertWithMessage:[error localizedDescription]];
            };
            
            [client POST:@"users/sign_in"
              parameters:params
                 success:success
                 failure:failure];
        });
    }
}

- (void)_forgetPasswordButtonPressed:(UIButton *)sender
{
    ISForgetPasswordViewController *forgetPasswordViewController = [[ISForgetPasswordViewController alloc] init];
    [self.navigationController pushViewController:forgetPasswordViewController
                                         animated:YES];
}

- (void)_cancelButtonPressed:(id)sender
{
    [self mz_dismissFormSheetControllerAnimated:YES
                              completionHandler:^(MZFormSheetController *formSheetController) {
                                  [self.view endEditing:YES];
                              }];
}

- (BOOL)_isValid
{
    if ([NSString isNilOrEmpty:self.emailTextField.text]) {
        [ISAppStyleSheet showAlertWithMessage:NSLocalizedString(@"Please enter your Email.", @"ISSigninViewController")];
        return NO;
    } else if (! [self.emailTextField.text isMatchedByRegex:IS_EMAIL_REGEX]) {
        [ISAppStyleSheet showAlertWithMessage:NSLocalizedString(@"Please enter valid Email.", @"ISSignupViewController")];
        return NO;
    } else if ([NSString isNilOrEmpty:self.passwordTextField.text]) {
        [ISAppStyleSheet showAlertWithMessage:NSLocalizedString(@"Please enter your Password.", @"ISSigninViewController")];
        return NO;
    } else if (self.passwordTextField.text.length < 6) {
        [ISAppStyleSheet showAlertWithMessage:NSLocalizedString(@"Password should be at least 6 characters.", @"ISSigninViewController")];
        return NO;
    }
    return YES;
}

#pragma mark - TextField delegate method

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.emailTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    return YES;
}

@end
