//
//  ISSignupViewController.m
//  iSignin
//
//  Created by Damon Yuan on 22/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISSignupViewController.h"
#import "ISGatewayClient.h"
#import "RegexKitLite.h"
#import "MZFormSheetController.h"
#import "AppDelegate.h"

@interface ISSignupViewController ()

@property (nonatomic, strong) FUITextField *emailTextField;
@property (nonatomic, strong) FUITextField *usernameTextField;
@property (nonatomic, strong) FUITextField *passwordTextField;
@property (nonatomic, strong) FUITextField *confirmationTextField;
@property (nonatomic, strong) FUIButton    *signupButton;
@property (nonatomic, strong) MBProgressHUD *hud;

@end

@implementation ISSignupViewController

- (void)dealloc
{
    _emailTextField.delegate        = nil;
    _usernameTextField.delegate     = nil;
    _passwordTextField.delegate     = nil;
    _confirmationTextField.delegate = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self _setupNavigationbar];
    [self _setupLayout];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.emailTextField becomeFirstResponder];
}

- (void)_setupNavigationbar
{
    self.title = NSLocalizedString(@"Sign Up", @"ISSignupViewController");
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                  target:self
                                                                                  action:@selector(_cancelButtonPressed:)];
    cancelButton.accessibilityLabel = @"cancel sign up button";
    self.navigationItem.leftBarButtonItem = cancelButton;
}

- (void)_setupLayout
{
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [self.view addSubview:self.emailTextField];
    [self.view addSubview:self.usernameTextField];
    [self.view addSubview:self.passwordTextField];
    [self.view addSubview:self.confirmationTextField];
    [self.view addSubview:self.signupButton];
    
    self.emailTextField.translatesAutoresizingMaskIntoConstraints        = NO;
    self.usernameTextField.translatesAutoresizingMaskIntoConstraints     = NO;
    self.passwordTextField.translatesAutoresizingMaskIntoConstraints     = NO;
    self.confirmationTextField.translatesAutoresizingMaskIntoConstraints = NO;
    self.signupButton.translatesAutoresizingMaskIntoConstraints          = NO;
    
    [self.view setNeedsUpdateConstraints];
    
}

- (void)updateViewConstraints
{
    [super updateViewConstraints];
    
    NSDictionary *views = @{@"emailTextField"        : self.emailTextField,
                            @"usernameTextField"     : self.usernameTextField,
                            @"passwordTextField"     : self.passwordTextField,
                            @"confirmationTextField" : self.confirmationTextField,
                            @"signupButton"          : self.signupButton};
    NSArray *constraints = nil;
    
    if (IS_PAD) {
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-60-[emailTextField]-60-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-60-[usernameTextField]-60-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-60-[passwordTextField]-60-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-60-[confirmationTextField]-60-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-60-[signupButton]-60-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[emailTextField(40)]-20-[usernameTextField(40)]-20-[passwordTextField(40)]-20-[confirmationTextField(40)]-20-[signupButton(40)]-20-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
    } else {
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[emailTextField]-20-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[usernameTextField]-20-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[passwordTextField]-20-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[confirmationTextField]-20-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[signupButton]-20-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[emailTextField(40)]-20-[usernameTextField(40)]-20-[passwordTextField(40)]-20-[confirmationTextField(40)]-20-[signupButton(40)]-20-|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.view addConstraints:constraints];
        
    }
}

#pragma mark - getter

- (FUITextField *)emailTextField
{
    if (! _emailTextField) {
        _emailTextField                    = [[FUITextField alloc] init];
        _emailTextField.delegate           = self;
        _emailTextField.keyboardType       = UIKeyboardTypeEmailAddress;
        _emailTextField.font               = [UIFont flatFontOfSize:16];
        _emailTextField.textFieldColor     = [UIColor cloudsColor];
        _emailTextField.backgroundColor    = [ISAppStyleSheet defaultBackgroundColor];
        _emailTextField.edgeInsets         = UIEdgeInsetsMake(4.f,15.f, 4.f, 15.f);
        _emailTextField.borderColor        = [UIColor turquoiseColor];
        _emailTextField.borderWidth        = 2.f;
        _emailTextField.cornerRadius       = 3.f;
        _emailTextField.placeholder        = NSLocalizedString(@"Email", @"ISSignupViewController");
        _emailTextField.accessibilityLabel = @"Email text field";
    }
    return _emailTextField;
}

- (FUITextField *)usernameTextField
{
    if (! _usernameTextField) {
        _usernameTextField                    = [[FUITextField alloc] init];
        _usernameTextField.delegate           = self;
        _usernameTextField.keyboardType       = UIKeyboardTypeDefault;
        _usernameTextField.font               = [UIFont flatFontOfSize:16];
        _usernameTextField.textFieldColor     = [UIColor cloudsColor];
        _usernameTextField.backgroundColor    = [ISAppStyleSheet defaultBackgroundColor];
        _usernameTextField.edgeInsets         = UIEdgeInsetsMake(4.f,15.f, 4.f, 15.f);
        _usernameTextField.borderColor        = [UIColor turquoiseColor];
        _usernameTextField.borderWidth        = 2.f;
        _usernameTextField.cornerRadius       = 3.f;
        _usernameTextField.placeholder        = NSLocalizedString(@"Username", @"ISSignupViewController");
        _usernameTextField.accessibilityLabel = @"Username text field";
    }
    return _usernameTextField;
}

- (FUITextField *)passwordTextField
{
    if (! _passwordTextField) {
        _passwordTextField                    = [[FUITextField alloc] init];
        _passwordTextField.delegate           = self;
        _passwordTextField.secureTextEntry    = YES;
        _passwordTextField.font               = [UIFont flatFontOfSize:16];
        _passwordTextField.textFieldColor     = [UIColor cloudsColor];
        _passwordTextField.backgroundColor    = [ISAppStyleSheet defaultBackgroundColor];
        _passwordTextField.edgeInsets         = UIEdgeInsetsMake(4.f,15.f, 4.f, 15.f);
        _passwordTextField.borderColor        = [UIColor turquoiseColor];
        _passwordTextField.borderWidth        = 2.f;
        _passwordTextField.cornerRadius       = 3.f;
        _passwordTextField.placeholder        = NSLocalizedString(@"Password (at lease 6 characters)", @"ISSignupViewController");
        _passwordTextField.accessibilityLabel = @"Password text field";
    }
    return _passwordTextField;
}

- (FUITextField *)confirmationTextField
{
    if (! _confirmationTextField) {
        _confirmationTextField                    = [[FUITextField alloc] init];
        _confirmationTextField.delegate           = self;
        _confirmationTextField.secureTextEntry    = YES;
        _confirmationTextField.font               = [UIFont flatFontOfSize:16];
        _confirmationTextField.textFieldColor     = [UIColor cloudsColor];
        _confirmationTextField.backgroundColor    = [ISAppStyleSheet defaultBackgroundColor];
        _confirmationTextField.edgeInsets         = UIEdgeInsetsMake(4.f,15.f, 4.f, 15.f);
        _confirmationTextField.borderColor        = [UIColor turquoiseColor];
        _confirmationTextField.borderWidth        = 2.f;
        _confirmationTextField.cornerRadius       = 3.f;
        _confirmationTextField.placeholder        = NSLocalizedString(@"Confirmation", @"ISSignupViewController");
        _confirmationTextField.accessibilityLabel = @"Confirmation text field";
    }
    return _confirmationTextField;
}

- (FUIButton *)signupButton
{
    if (! _signupButton) {
        _signupButton = [[FUIButton alloc] init];
        _signupButton.buttonColor  = [UIColor turquoiseColor];
        _signupButton.shadowColor  = [UIColor greenSeaColor];
        _signupButton.shadowHeight = 2.f;
        _signupButton.cornerRadius = 6.f;
        _signupButton.titleLabel.font = [UIFont boldSystemFontOfSize:16.f];
        [_signupButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_signupButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [_signupButton setTitle:NSLocalizedString(@"Sign up", @"ISSignupViewController") forState:UIControlStateNormal];
        _signupButton.accessibilityLabel = @"Sign up button";
        [_signupButton addTarget:self
                          action:@selector(_signupButtonPressed:)
                forControlEvents:UIControlEventTouchUpInside];
    }
    return _signupButton;
}

- (MBProgressHUD *)hud
{
    if (! _hud) {
        _hud = [[MBProgressHUD alloc] initWithView:self.view];
        _hud.userInteractionEnabled = NO;
        [self.view addSubview:_hud];
    }
    return _hud;
}

#pragma mark - Selector methods

- (void)_signupButtonPressed:(id)sender
{
    [self.view endEditing:YES];
    if ([self _isValid])
    {
        [self.hud show:YES];
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            ISGatewayClient *client = [ISGatewayClient sharedClient];
            NSDictionary *params = @{@"user" : @{@"email"                 : self.emailTextField.text,
                                                 @"username"              : self.usernameTextField.text,
                                                 @"password"              : self.passwordTextField.text,
                                                 @"password_confirmation" : self.confirmationTextField.text}};
            ISHTTPClientSuccessBlock success = ^(NSURLSessionDataTask *task, id responseObject)
            {
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                [self.hud hide:YES];
                [ISAppStyleSheet showAlertWithTitle:NSLocalizedString(@"Congratulations!", @"ISSignupViewController")
                                            message:NSLocalizedString(@"An email has been forward to your address, and after confirmation you can sign in with your account!", @"ISSignupViewController")];
            };
            
            ISHTTPClientFailureBlock failure = ^(NSURLSessionDataTask *task, NSError *error)
            {
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                [self.hud hide:YES];
                [ISAppStyleSheet showAlertWithMessage:[error localizedDescription]];
            };
            
            [client POST:@"users/sign_up"
              parameters:params
                 success:success
                 failure:failure];
        });
    }
}

- (void)_cancelButtonPressed:(id)sender
{
    [self mz_dismissFormSheetControllerAnimated:YES
                              completionHandler:^(MZFormSheetController *formSheetController) {
                                  [self.view endEditing:YES];
                              }];
}

- (BOOL)_isValid
{
    if ([NSString isNilOrEmpty:self.emailTextField.text]) {
        [ISAppStyleSheet showAlertWithMessage:NSLocalizedString(@"Please enter your Email.", @"ISSignupViewController")];
        return NO;
    } else if (! [self.emailTextField.text isMatchedByRegex:IS_EMAIL_REGEX]) {
        [ISAppStyleSheet showAlertWithMessage:NSLocalizedString(@"Please enter valid Email.", @"ISSignupViewController")];
        return NO;
    } else if ([NSString isNilOrEmpty:self.usernameTextField.text]) {
        [ISAppStyleSheet showAlertWithMessage:NSLocalizedString(@"Please enter your Username.", @"ISSignupViewController")];
        return NO;
    } else if ([NSString isNilOrEmpty:self.passwordTextField.text]) {
        [ISAppStyleSheet showAlertWithMessage:NSLocalizedString(@"Please enter your Password.", @"ISSignupViewController")];
        return NO;
    } else if ([NSString isNilOrEmpty:self.confirmationTextField.text]) {
        [ISAppStyleSheet showAlertWithMessage:NSLocalizedString(@"Please enter your Password Confirmation.", @"ISSignupViewController")];
        return NO;
    } else if (self.passwordTextField.text.length < 6) {
        [ISAppStyleSheet showAlertWithMessage:NSLocalizedString(@"Password should be at least 6 characters.", @"ISSignupViewController")];
        return NO;
    } else if (! [self.confirmationTextField.text isEqualToString:self.passwordTextField.text]) {
        [ISAppStyleSheet showAlertWithMessage:NSLocalizedString(@"Confirmation should be the same as Password.", @"ISSignupViewController")];
        return NO;
    }
    return YES;
}

#pragma mark - TextField delegate method

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.emailTextField) {
        [self.usernameTextField becomeFirstResponder];
    } else if (textField == self.usernameTextField) {
        [self.passwordTextField becomeFirstResponder];
    } else if (textField == self.passwordTextField) {
        [self.confirmationTextField becomeFirstResponder];
    }
    return YES;
}



@end
