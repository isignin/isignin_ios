//
//  Event.m
//  iSignin
//
//  Created by Damon Yuan on 18/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "Event.h"
#import "User.h"


@implementation Event

@dynamic event_id;
@dynamic name;
@dynamic owner;
@dynamic participates;

@end
