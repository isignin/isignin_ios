//
//  User.h
//  iSignin
//
//  Created by Damon Yuan on 18/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;
@class Event;

@interface User : NSManagedObject

@property (nonatomic, retain) NSString * address1;
@property (nonatomic, retain) NSString * address2;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSNumber * user_id;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * telephone;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSSet *followers;
@property (nonatomic, retain) NSSet *following;
@property (nonatomic, retain) NSSet *owned_events;
@property (nonatomic, retain) NSSet *participated_events;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addFollowersObject:(User *)value;
- (void)removeFollowersObject:(User *)value;
- (void)addFollowers:(NSSet *)values;
- (void)removeFollowers:(NSSet *)values;

- (void)addFollowingObject:(User *)value;
- (void)removeFollowingObject:(User *)value;
- (void)addFollowing:(NSSet *)values;
- (void)removeFollowing:(NSSet *)values;

- (void)addOwned_eventsObject:(Event *)value;
- (void)removeOwned_eventsObject:(Event *)value;
- (void)addOwned_events:(NSSet *)values;
- (void)removeOwned_events:(NSSet *)values;

- (void)addParticipated_eventsObject:(Event *)value;
- (void)removeParticipated_eventsObject:(Event *)value;
- (void)addParticipated_events:(NSSet *)values;
- (void)removeParticipated_events:(NSSet *)values;

@end
