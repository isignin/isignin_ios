//
//  Event.h
//  iSignin
//
//  Created by Damon Yuan on 18/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface Event : NSManagedObject

@property (nonatomic, retain) NSNumber * event_id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) User *owner;
@property (nonatomic, retain) NSSet *participates;
@end

@interface Event (CoreDataGeneratedAccessors)

- (void)addParticipatesObject:(User *)value;
- (void)removeParticipatesObject:(User *)value;
- (void)addParticipates:(NSSet *)values;
- (void)removeParticipates:(NSSet *)values;

@end
