//
//  User.m
//  iSignin
//
//  Created by Damon Yuan on 18/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "User.h"
#import "Event.h"


@implementation User

@dynamic address1;
@dynamic address2;
@dynamic city;
@dynamic country;
@dynamic email;
@dynamic user_id;
@dynamic phone;
@dynamic state;
@dynamic telephone;
@dynamic title;
@dynamic username;
@dynamic followers;
@dynamic following;
@dynamic owned_events;
@dynamic participated_events;

@end
