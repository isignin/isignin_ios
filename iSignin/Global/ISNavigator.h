//
//  ISNavigator.h
//  iSignin
//
//  Created by Damon Yuan on 7/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ISNavigator : NSObject

+ (BOOL)needLogin;

+ (void)logout;
+ (void)loginWithAccount:(NSString *)account
                   token:(NSString *)token;

+ (void)updateRootViewControllerWithWelcomeViewControllerWithSplash:(BOOL)isSplash;
+ (void)updateRootViewControllerWithHomeViewController;

@end
