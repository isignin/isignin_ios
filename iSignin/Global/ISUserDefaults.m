//
//  ISUserDefaults.m
//  iSignin
//
//  Created by Damon Yuan on 7/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISUserDefaults.h"

NSString * const kCurrentUserAccount = @"kCurrentUserAccount";

@implementation ISUserDefaults

+ (NSString *)currentUserAccount
{
    return [self defaultValueForKey:kCurrentUserAccount];
}

#pragma mark - Helpers

+ (void)setDefaultValue:(id)v forKey:(NSString *)k
{
    [[NSUserDefaults standardUserDefaults] setObject:v forKey:k];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (id)defaultValueForKey:(NSString *)k
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:k];
}

+ (void)removeValueForKey:(NSString *)k
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:k];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
