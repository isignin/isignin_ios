//
//  ISConfiguration.m
//  iSignin
//
//  Created by Damon Yuan on 7/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISConfiguration.h"

// Base URL
#if   DB_ENVIRONMENT == DB_ENVIRONMENT_DEBUG_LOCAL
NSString * const DataCenterUrl = @"http://localhost:3000/api/";
NSString * const API_AUTH_NAME = @"damonyuan";
NSString * const API_AUTH_PASSWORD = @"DKCDQUQXNKPALJFVBGAJGTOYIFZSBSDEQMSNPXYHQLCAFNMXPCJSXKZSKGVPVUBD";
NSString * const HockeyappBetaID    = @"43729db61be36130bfe0f40a29aa43e1";
NSString * const HockeyappAppLiveID = @"077b4b4be230ed25a6fc96f46441b156";
#elif DB_ENVIRONMENT == DB_ENVIRONMENT_DEBUG_SERVER
NSString * const DataCenterUrl = @"https://isigninstaging.herokuapp.com/api/";
NSString * const API_AUTH_NAME = @"damonyuan";
NSString * const API_AUTH_PASSWORD = @"DKCDQUQXNKPALJFVBGAJGTOYIFZSBSDEQMSNPXYHQLCAFNMXPCJSXKZSKGVPVUBD";
NSString * const HockeyappBetaID    = @"43729db61be36130bfe0f40a29aa43e1";
NSString * const HockeyappAppLiveID = @"077b4b4be230ed25a6fc96f46441b156";
#elif DB_ENVIRONMENT == DB_ENVIRONMENT_ALPHA
NSString * const DataCenterUrl = @"https://isigninstaging.herokuapp.com/api/";
NSString * const API_AUTH_NAME = @"damonyuan";
NSString * const API_AUTH_PASSWORD  = @"DKCDQUQXNKPALJFVBGAJGTOYIFZSBSDEQMSNPXYHQLCAFNMXPCJSXKZSKGVPVUBD";
NSString * const HockeyappBetaID    = @"43729db61be36130bfe0f40a29aa43e1";
NSString * const HockeyappAppLiveID = @"077b4b4be230ed25a6fc96f46441b156";
#elif DB_ENVIRONMENT == DB_ENVIRONMENT_BETA
NSString * const DataCenterUrl = @"https://isigninbeta.herokuapp.com/api/";
NSString * const API_AUTH_NAME = @"damonyuan";
NSString * const API_AUTH_PASSWORD  = @"DKCDQUQXNKPALJFVBGAJGTOYIFZSBSDEQMSNPXYHQLCAFNMXPCJSXKZSKGVPVUBD";
NSString * const HockeyappBetaID    = @"4397ee07dd6be2078c6070e0d6fa7991";
NSString * const HockeyappAppLiveID = @"077b4b4be230ed25a6fc96f46441b156";
#elif DB_ENVIRONMENT == DB_ENVIRONMENT_APPSTORE
NSString * const DataCenterUrl = @"";
NSString * const API_AUTH_NAME = @"";
NSString * const API_AUTH_PASSWORD  = @"";
NSString * const HockeyappBetaID    = @"4397ee07dd6be2078c6070e0d6fa7991";
NSString * const HockeyappAppLiveID = @"077b4b4be230ed25a6fc96f46441b156";
#endif

NSString * const kISAppTypeHeaderField = @"kISAppTypeHeaderField";
NSString * const kISDeviceModelHeaderField = @"kISDeviceModelHeaderField";
NSString * const kISAppVersionHeaderField = @"kISAppVersionHeaderField";
NSString * const kISSystemVersionHeaderField = @"kISSystemVersionHeaderField";
NSString * const kISTimeZoneHeaderField = @"kISTimeZoneHeaderField";
NSString * const kISCountryHeaderField = @"kISCountryHeaderField";

NSString * const IS_ERROR_DOMAIN     = @"com.iSignin.httpError";
NSInteger  const IS_ERROR_LOGIN_FAIL = 400;
NSInteger  const IS_ERROR_NEED_LOGIN = 401;
NSInteger  const IS_ERROR_NOT_FOUND  = 404;
NSInteger  const IS_ERROR_HTTP_ERROR = 500;

NSString * const IS_EMAIL_REGEX = @"([a-zA-Z0-9%_.+\\-]+)@([a-zA-Z0-9.\\-]+?\\.[a-zA-Z]{2,6})";

NSString * const IS_KEYCHAIN_TOKEN = @"iSigninToken";

NSString * const kISWelcomeViewShouldResetTimer = @"kISWelcomeViewShouldResetTimer";

@implementation ISConfiguration

+ (NSString *)ISAppVersion
{
    return [NSString stringWithFormat:@"%@ (%@)", [ISConfiguration ISAppShortVersion], [ISConfiguration ISAppBundleVersion]];
}

+ (NSString *)ISAppShortVersion
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}

+ (NSString *)ISAppBundleVersion
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
}

@end
