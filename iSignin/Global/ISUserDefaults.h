//
//  ISUserDefaults.h
//  iSignin
//
//  Created by Damon Yuan on 7/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ISUserDefaults : NSObject

extern NSString * const kCurrentUserAccount;

+ (NSString *)currentUserAccount;

+ (void)setDefaultValue:(id)v forKey:(NSString *)k;
+ (id)defaultValueForKey:(NSString *)k;
+ (void)removeValueForKey:(NSString *)k;

@end
