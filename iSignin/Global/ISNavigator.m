//
//  ISNavigator.m
//  iSignin
//
//  Created by Damon Yuan on 7/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISNavigator.h"
#import "ISWelcomeViewController.h"
#import "ISRootViewController.h"
#import "AppDelegate.h"
#import "SSKeychain.h"

@implementation ISNavigator

#pragma mark - User session

+ (void)logout
{
    [ISNavigator clearCurrentUser];
    [ISNavigator updateRootViewControllerWithWelcomeViewControllerWithSplash:NO];
}

+ (void)loginWithAccount:(NSString *)account
                   token:(NSString *)token
{
    [ISNavigator setCurrentUserAccount:account
                                 token:token];
    [ISNavigator updateRootViewControllerWithHomeViewController];
}

#pragma mark - Private

+ (void)setCurrentUserAccount:(NSString *)account
                        token:(NSString *)token
{
    [ISUserDefaults setDefaultValue:account
                             forKey:kCurrentUserAccount];
    [SSKeychain setPassword:token
                 forService:IS_KEYCHAIN_TOKEN account:account];
}

+ (void)clearCurrentUser
{
    [SSKeychain deletePasswordForService:IS_KEYCHAIN_TOKEN
                                 account:[ISUserDefaults defaultValueForKey:kCurrentUserAccount]];
    [ISUserDefaults removeValueForKey:kCurrentUserAccount];
}

#pragma mark - Helper

+ (BOOL)needLogin
{
    NSString *currentAccount = [ISUserDefaults defaultValueForKey:kCurrentUserAccount];
    if ([NSString isNilOrEmpty:currentAccount]) {
        return YES;
    }
    return NO;
}

#pragma mark - Update root view controller

+ (void)updateRootViewControllerWithViewController:(UIViewController *)viewController
{
    is_dispatch_main_async(^{
        [[[AppDelegate sharedAppDelegate] window] setRootViewController:viewController];
    });
}

+ (void)updateRootViewControllerWithWelcomeViewControllerWithSplash:(BOOL)isSplash
{
    ISWelcomeViewController *welcomeViewController = [[ISWelcomeViewController alloc] initWithSplash:isSplash];
    [self updateRootViewControllerWithViewController:welcomeViewController];
}

+ (void)updateRootViewControllerWithHomeViewController
{
    ISRootViewController *isRootViewController = [[UIStoryboard storyboardWithName:@"ISRoot" bundle:nil] instantiateViewControllerWithIdentifier:@"rootViewController"];
    [self updateRootViewControllerWithViewController:isRootViewController];
}

@end
