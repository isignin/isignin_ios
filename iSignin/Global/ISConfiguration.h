//
//  ISConfiguration.h
//  iSignin
//
//  Created by Damon Yuan on 7/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DB_ENVIRONMENT_DEBUG_LOCAL 0
#define DB_ENVIRONMENT_DEBUG_SERVER 1
#define DB_ENVIRONMENT_ALPHA 2
#define DB_ENVIRONMENT_BETA 3
#define DB_ENVIRONMENT_APPSTORE 4

#ifndef DB_ENVIRONMENT
#define DB_ENVIRONMENT DB_ENVIRONMENT_DEVELOPMENT
#endif

extern NSString * const DataCenterUrl;
extern NSString * const API_AUTH_NAME;
extern NSString * const API_AUTH_PASSWORD;

extern NSString * const HockeyappBetaID;
extern NSString * const HockeyappAppLiveID;

extern NSString * const kISAppTypeHeaderField;
extern NSString * const kISDeviceModelHeaderField;
extern NSString * const kISAppVersionHeaderField;
extern NSString * const kISSystemVersionHeaderField;
extern NSString * const kISTimeZoneHeaderField;
extern NSString * const kISCountryHeaderField;

extern NSString * const IS_ERROR_DOMAIN;
extern NSInteger  const IS_ERROR_LOGIN_FAIL;
extern NSInteger  const IS_ERROR_NEED_LOGIN;
extern NSInteger  const IS_ERROR_NOT_FOUND;
extern NSInteger  const IS_ERROR_HTTP_ERROR;

extern NSString * const IS_EMAIL_REGEX;

extern NSString * const IS_KEYCHAIN_TOKEN;

extern NSString * const kISWelcomeViewShouldResetTimer;

@interface ISConfiguration : NSObject

+ (NSString *)ISAppVersion;
+ (NSString *)ISAppShortVersion;
+ (NSString *)ISAppBundleVersion;

@end
