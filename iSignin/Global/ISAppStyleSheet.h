//
//  ISAppStyleSheet.h
//  iSignin
//
//  Created by Damon Yuan on 7/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ISAppStyleSheet : NSObject

#define kISSeparatorThick 1/[UIScreen mainScreen].scale

+ (UIColor *)defaultBackgroundColor;
+ (UIColor *)colorOfStroke;

+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
                  delegate:(id)delegate
         cancelButtonTitle:(NSString *)cancelButtonTitle
         otherButtonTitles:(NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;

+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message;

+ (void)showAlertWithMessage:(NSString *)message;

+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
          otherButtonTitle:(NSString *)otherButtonTitle;

@end
