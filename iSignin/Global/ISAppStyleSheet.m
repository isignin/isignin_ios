//
//  ISAppStyleSheet.m
//  iSignin
//
//  Created by Damon Yuan on 7/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISAppStyleSheet.h"

@implementation ISAppStyleSheet

+ (UIColor *)defaultBackgroundColor
{
    static UIColor *defaultBgColor = nil;
    static dispatch_once_t bgToken;
    dispatch_once(&bgToken, ^{
        defaultBgColor = UIColorFromRGB(0xF0F0F0);
    });
    return defaultBgColor;
}

+ (UIColor *)colorOfStroke
{
    static UIColor *strokeColor = nil;
    static dispatch_once_t strokeColorToken;
    dispatch_once(&strokeColorToken, ^{
        strokeColor = UIColorFromRGB(0xCCCCCC);
    });
    return strokeColor;
}

#pragma mark - Alert
+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
                  delegate:(id)delegate
         cancelButtonTitle:(NSString *)cancelButtonTitle
         otherButtonTitles:(NSString *)otherButtonTitles, ...
{
    FUIAlertView *alertView = [[FUIAlertView alloc] initWithTitle:title
                                                          message:message
                                                         delegate:delegate
                                                cancelButtonTitle:cancelButtonTitle
                                                otherButtonTitles:otherButtonTitles, nil];
    alertView.titleLabel.textColor              = [UIColor cloudsColor];
    alertView.titleLabel.font                   = [UIFont boldFlatFontOfSize:16];
    alertView.messageLabel.textColor            = [UIColor cloudsColor];
    alertView.messageLabel.font                 = [UIFont flatFontOfSize:14];
    alertView.backgroundOverlay.backgroundColor = [[UIColor cloudsColor] colorWithAlphaComponent:0.8];
    alertView.alertContainer.backgroundColor    = [UIColor midnightBlueColor];
    alertView.defaultButtonColor                = [UIColor cloudsColor];
    alertView.defaultButtonShadowColor          = [UIColor asbestosColor];
    alertView.defaultButtonFont                 = [UIFont boldFlatFontOfSize:16];
    alertView.defaultButtonTitleColor           = [UIColor asbestosColor];
    [alertView show];
}

+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
          otherButtonTitle:(NSString *)otherButtonTitle
{
    [ISAppStyleSheet showAlertWithTitle:title
                                message:message
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", @"ISAppStyleSheet")
                      otherButtonTitles:otherButtonTitle, nil];
}

+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
{
    [ISAppStyleSheet showAlertWithTitle:title
                                message:message
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", @"ISAppStyleSheet")
                      otherButtonTitles:nil];
}

+ (void)showAlertWithMessage:(NSString *)message
{
    [ISAppStyleSheet showAlertWithTitle:NSLocalizedString(@"Alert", @"ISAppStyleSheet")
                                message:message
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", @"ISAppStyleSheet")
                      otherButtonTitles:nil];

}



@end
