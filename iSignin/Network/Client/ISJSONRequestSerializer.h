//
//  ISJSONRequestSerializer.h
//  iSignin
//
//  Created by Damon Yuan on 22/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "AFURLRequestSerialization.h"

@interface ISJSONRequestSerializer : AFJSONRequestSerializer

+ (instancetype)defaultSerializer;

@end
