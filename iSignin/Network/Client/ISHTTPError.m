//
//  ISHTTPError.m
//  iSignin
//
//  Created by Damon Yuan on 22/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISHTTPError.h"
#import "ISJSONRequestSerializer.h"

@implementation ISHTTPError

#pragma mark - NSURLSessionTask Error

+ (ISHTTPError *)errorFromURLSessionTask:(NSURLSessionTask *)task {
    return [self errorFromURLSessionTask:task originalError:nil];
}

+ (ISHTTPError *)errorFromURLSessionTask:(NSURLSessionTask *)task
                           originalError:(NSError *)originalError
{
    if ([originalError.domain isEqualToString:@"com.iSignin.httpError"])
    {
        return (ISHTTPError *)originalError;
    }
    
    if (task.state == NSURLSessionTaskStateCanceling) {
        return nil;
    }
    
    if (originalError.code == NSURLErrorCancelled) {
        return nil;
    }
    
    if (originalError.code == NSURLErrorCannotFindHost ||
        originalError.code == NSURLErrorCannotConnectToHost ||
        originalError.code == NSURLErrorNetworkConnectionLost ||
        originalError.code == NSURLErrorDNSLookupFailed ||
        originalError.code == NSURLErrorNotConnectedToInternet) {
        return [self errorWithDomain:originalError.domain
                                code:originalError.code
                            userInfo:@{NSLocalizedDescriptionKey: NSLocalizedString(@"Unable to connect to server. Please check your internet connection.", @"ISHTTPError")}];
    }
    
    if (task.error && task.error.code == NSURLErrorTimedOut) {
        return [self timeoutError];
    }
    
    ISHTTPError *error = nil;
    NSData *responseData = originalError.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    error = [self specificHttpErrorFromResponse:(NSHTTPURLResponse *)task.response
                             responseDictionary:[responseData JSONDataToDictionary]];
    if (error) {
        return error;
    }
    
    if (task.error) {
        return [self errorWithDomain:IS_ERROR_DOMAIN
                                code:NSURLErrorCannotOpenFile
                            userInfo:[NSDictionary dictionaryWithObject:[task.error localizedDescription] forKey:NSLocalizedDescriptionKey]];
    }
    
    return [self unknownErrorWithURL:task.currentRequest.URL
                          statusCode:((NSHTTPURLResponse *)task.response).statusCode
                         responseDic:[responseData JSONDataToDictionary]];
}

#pragma mark - AFHTTPRequestOperation Error

+ (ISHTTPError *)errorFromAFHTTPRequestOperation:(AFHTTPRequestOperation *)operation
{
    if (operation.isCancelled) {
        return nil;
    }
    if (operation.error && operation.error.code == NSURLErrorTimedOut) {
        return [self timeoutError];
    }
    
    if (operation.error.code == NSURLErrorCannotFindHost ||
        operation.error.code == NSURLErrorCannotConnectToHost ||
        operation.error.code == NSURLErrorNetworkConnectionLost ||
        operation.error.code == NSURLErrorDNSLookupFailed ||
        operation.error.code == NSURLErrorNotConnectedToInternet) {
        return [self errorWithDomain:operation.error.domain
                                code:operation.error.code
                            userInfo:@{NSLocalizedDescriptionKey: NSLocalizedString(@"Unable to connect to server. Please check your internet connection.", @"ISHTTPError")}];
    }
    
    ISHTTPError *error = [self specificHttpErrorFromResponse:operation.response
                                          responseDictionary:[operation.responseData JSONDataToDictionary]];
    if (error) {
        return error;
    }
    
    if (operation.error) {
        return [self errorWithDomain:IS_ERROR_DOMAIN
                                code:NSURLErrorCannotOpenFile
                            userInfo:[NSDictionary dictionaryWithObject:[operation.error localizedDescription]
                                                                 forKey:NSLocalizedDescriptionKey]];
    }
    
    return [self unknownErrorWithURL:operation.request.URL
                          statusCode:operation.response.statusCode
                         responseDic:[operation.responseString JSONStringToDictionary]];
}


#pragma mark - Timeout Error

+ (ISHTTPError *)timeoutError
{
    return [self errorWithDomain:IS_ERROR_DOMAIN
                            code:NSURLErrorTimedOut
                        userInfo:[NSDictionary dictionaryWithObject:NSLocalizedString(@"Request time out, please retry.", @"ISHTTPError")
                                                             forKey:NSLocalizedDescriptionKey]];
}

#pragma mark - Unknown Error

+ (ISHTTPError *)unknownErrorWithURL:(NSURL *)url
                          statusCode:(NSInteger)statusCode
                         responseDic:(NSDictionary *)responseDic
{
    ISHTTPError *error = [self errorWithDomain:IS_ERROR_DOMAIN
                                          code:kCFNetServiceErrorUnknown
                                      userInfo:[NSDictionary dictionaryWithObject:NSLocalizedString(@"Unknown error.", @"ISHTTPError")
                                                                           forKey:NSLocalizedDescriptionKey]];
    
    return error;
}


#pragma mark - Error from specific HTTP status codes

+ (ISHTTPError *)specificHttpErrorFromResponse:(NSHTTPURLResponse *)response
                            responseDictionary:(NSDictionary *)responseDictionary
{
    if (response.statusCode < 400 && response.statusCode >= 200) {
        return nil;
    }
    
    NSDictionary *errorInfo = responseDictionary;
    NSInteger errorCode = response.statusCode;
    NSString *errorMessage;
    if ([responseDictionary valueForKey:@"data"]) {
        errorMessage = [self errorString:errorInfo[@"data"]];
    } else {
        errorMessage = [self errorString:errorInfo];
    }
    
    
    if (nil == errorMessage || [errorMessage isEqual:[NSNull null]]) {
        return nil;
    }
    
    ISHTTPError *error = nil;
    
    if (response.statusCode == 400) {
        
        error = [self errorWithDomain:IS_ERROR_DOMAIN
                                 code:IS_ERROR_LOGIN_FAIL
                             userInfo:[NSDictionary dictionaryWithObject:errorMessage
                                                                  forKey:NSLocalizedDescriptionKey]];
        
    } else if (response.statusCode == 401) {
        error = [self errorWithDomain:IS_ERROR_DOMAIN
                                 code:IS_ERROR_NEED_LOGIN
                             userInfo:[NSDictionary dictionaryWithObject:errorMessage
                                                                  forKey:NSLocalizedDescriptionKey]];
    } else if (response.statusCode == 404) {
        error = [self errorWithDomain:IS_ERROR_DOMAIN
                                 code:IS_ERROR_NOT_FOUND
                             userInfo:[NSDictionary dictionaryWithObject:errorMessage
                                                                  forKey:NSLocalizedDescriptionKey]];
    } else if (response.statusCode == 500 || [errorMessage length] > 0) {
        error = [self errorWithDomain:IS_ERROR_DOMAIN
                                 code:errorCode
                             userInfo:[NSDictionary dictionaryWithObject:errorMessage
                                                                  forKey:NSLocalizedDescriptionKey]];
    }
    
    return error;
}

#pragma mark - Helper

+ (ISHTTPError *)alertError:(NSError *)error
                   fromTask:(NSURLSessionTask *)task
{
    ISHTTPError *processedError = [ISHTTPError errorFromURLSessionTask:task originalError:error];
    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert", @"ISHTTPError")
                                                 message:[processedError localizedDescription]
                                                delegate:nil
                                       cancelButtonTitle:NSLocalizedString(@"OK", @"ISHTTPError")
                                       otherButtonTitles:nil];
    [av show];
    
    return processedError;
}

+ (NSString *)errorString:(NSDictionary *)errorDictionary
{
    NSMutableString *errors = [[NSMutableString alloc] init];
    for (NSString *key in [errorDictionary allKeys]) {
        id value = [errorDictionary valueForKey:key];
        if ([value isKindOfClass:[NSString class]]) {
            [errors appendFormat:@"%@: %@\n", key, value];
        } else if ([value isKindOfClass:[NSDictionary class]]) {
            [errors appendFormat:@"%@: %@\n", key, [self errorString:value]];
        } else if ([value isKindOfClass:[NSArray class]]) {
            id firstValue = [value firstObject];
            if ([firstValue isKindOfClass:[NSString class]]) {
                [errors appendFormat:@"%@: %@\n", key, firstValue];
            }
        }
    }
    return [NSString stringWithString:errors];
}

@end
