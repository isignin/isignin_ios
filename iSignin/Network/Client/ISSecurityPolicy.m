//
//  ISSecurityPolicy.m
//  iSignin
//
//  Created by Damon Yuan on 22/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISSecurityPolicy.h"

@implementation ISSecurityPolicy

+ (instancetype)defaultPolicy
{
    ISSecurityPolicy *policy = [super defaultPolicy];
    policy.allowInvalidCertificates = YES;
    return policy;
}

@end
