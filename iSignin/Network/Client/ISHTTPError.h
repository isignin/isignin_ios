//
//  ISHTTPError.h
//  iSignin
//
//  Created by Damon Yuan on 22/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ISHTTPError : NSError

+ (ISHTTPError *)errorFromURLSessionTask:(NSURLSessionTask *)task;

+ (ISHTTPError *)errorFromURLSessionTask:(NSURLSessionTask *)task
                           originalError:(NSError *)originalError;

+ (ISHTTPError *)errorFromAFHTTPRequestOperation:(AFHTTPRequestOperation *)operation;

+ (ISHTTPError *)specificHttpErrorFromResponse:(NSHTTPURLResponse *)response
                            responseDictionary:(NSDictionary *)responseDic;

+ (ISHTTPError *)timeoutError;

+ (ISHTTPError *)unknownErrorWithURL:(NSURL *)url
                          statusCode:(NSInteger)statusCode
                         responseDic:(NSDictionary *)responseString;

+ (ISHTTPError *)alertError:(NSError *)error
                   fromTask:(NSURLSessionTask *)task;

@end
