//
//  ISJSONRequestSerializer.m
//  iSignin
//
//  Created by Damon Yuan on 22/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISJSONRequestSerializer.h"
#import <sys/utsname.h>
#import "SSKeychain.h"

static NSString *_appTypeName;

@implementation ISJSONRequestSerializer

+ (instancetype)defaultSerializer
{
    ISJSONRequestSerializer *requestSerializer = [super serializer];
    [requestSerializer setValue:[self appTypeName]
             forHTTPHeaderField:kISAppTypeHeaderField];
    [requestSerializer setValue:[self deviceName]
             forHTTPHeaderField:kISDeviceModelHeaderField];
    [requestSerializer setValue:[ISConfiguration ISAppVersion]
             forHTTPHeaderField:kISAppVersionHeaderField];
    [requestSerializer setValue:[self systemVersion]
             forHTTPHeaderField:kISSystemVersionHeaderField];
    [requestSerializer setValue:[self country]
             forHTTPHeaderField:kISCountryHeaderField];
    [requestSerializer setValue:@"application/vnd.isignin.v1"
             forHTTPHeaderField:@"Accept"];
    [requestSerializer setValue:@"application/json"
             forHTTPHeaderField:@"Content-Type"];
    [requestSerializer setValue:[self language]
             forHTTPHeaderField:@"HTTP_ACCEPT_LANGUAGE"];
    return requestSerializer;
}

// SingleAccessToken should be updated for each request
- (NSURLRequest *)requestBySerializingRequest:(NSURLRequest *)request
                               withParameters:(id)parameters
                                        error:(NSError * __autoreleasing *)error {
    
    if ([ISNavigator needLogin]) {
        [self setValue:[self _basicAuthString]
    forHTTPHeaderField:@"Authorization"];
    } else {
        [self setValue:[self _tokenAuthString]
    forHTTPHeaderField:@"Authorization"];
    }
    
    return [super requestBySerializingRequest:request
                               withParameters:parameters
                                        error:error];
}

- (NSString *)_basicAuthString
{
    NSString *basicString = [NSString stringWithFormat:@"%@:%@", API_AUTH_NAME, API_AUTH_PASSWORD];
    NSData *basicData = [basicString dataUsingEncoding:NSUTF8StringEncoding];
    return [NSString stringWithFormat:@"Basic %@", [basicData base64EncodedStringWithOptions:0]];
}

- (NSString *)_tokenAuthString
{
    NSString *token = [SSKeychain passwordForService:IS_KEYCHAIN_TOKEN
                                             account:[ISUserDefaults currentUserAccount]];
    return [NSString stringWithFormat:@"Token %@", token];
}

#pragma mark - Helper

+ (NSString *)appTypeName
{
    if (_appTypeName) {
        return _appTypeName;
    }
    
    switch (DB_ENVIRONMENT) {
        case DB_ENVIRONMENT_ALPHA:
            _appTypeName = @"iSignin-alpha";
            break;
            
        case DB_ENVIRONMENT_BETA:
            _appTypeName = @"iSignin-beta";
            break;
            
        case DB_ENVIRONMENT_APPSTORE:
            _appTypeName = @"iSignin";
            break;
            
        default:
            _appTypeName = @"iSignin-debug";
            break;
    }
    return _appTypeName;
}

+ (NSString *)deviceName
{
    struct utsname systemInfo;
    uname(&systemInfo);
    return [NSString stringWithFormat:@"%@ %@ %@",
            [[UIDevice currentDevice] name],
            [[UIDevice currentDevice] model],
            [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding]];
}

+ (NSString *)systemVersion
{
    return [NSString stringWithFormat:@"%@ %@",
            [[UIDevice currentDevice] systemName],
            [[UIDevice currentDevice] systemVersion]];
}

+ (NSString *)language
{
    NSArray *languageArray = [NSLocale preferredLanguages];
    NSString *language = [languageArray objectAtIndex:0];
    return language;
}

+ (NSString *)country
{
    NSLocale *locale = [NSLocale currentLocale];
    NSString *country = [locale localeIdentifier];
    return country;
}

@end
