//
//  ISJSONResponseSerializer.m
//  iSignin
//
//  Created by Damon Yuan on 22/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISJSONResponseSerializer.h"
#import "ISHTTPError.h"

@implementation ISJSONResponseSerializer

- (BOOL)validateResponse:(NSHTTPURLResponse *)response
                    data:(NSData *)data
                   error:(NSError * __autoreleasing *)error
{
    BOOL isValid = [super validateResponse:response data:data error:error];
    if (*error != nil) {
        NSMutableDictionary *userInfo = [(*error).userInfo mutableCopy];
        NSData *errorData = userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        if (errorData) {
            NSError *newError = [ISHTTPError specificHttpErrorFromResponse:userInfo[AFNetworkingOperationFailingURLResponseErrorKey]
                                                        responseDictionary:[errorData JSONDataToDictionary]];
            if (newError) {
                (*error) = newError;
            }
        }
    }
    return isValid;
}

@end
