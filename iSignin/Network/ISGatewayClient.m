//
//  GatewayClient.m
//  iSignin
//
//  Created by Damon Yuan on 22/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISGatewayClient.h"

@implementation ISGatewayClient

+ (ISGatewayClient *)sharedClient
{
    static ISGatewayClient * _sharedClient = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        _sharedClient = [[ISGatewayClient alloc] init];
    });
    return _sharedClient;
}

- (instancetype)init
{
    return [super initWithBaseURL:[NSURL URLWithString:DataCenterUrl]];
}

@end
