//
//  AppDelegate+Hockeyapp.h
//  iSignin
//
//  Created by Damon Yuan on 8/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "AppDelegate.h"
#import <HockeySDK/HockeySDK.h>

@interface AppDelegate (Hockeyapp)
<
    BITHockeyManagerDelegate,
    BITUpdateManagerDelegate,
    BITCrashManagerDelegate
>

- (void)setupHockeyapp;
- (void)delayCheckCrashedInLastSession;

@end
