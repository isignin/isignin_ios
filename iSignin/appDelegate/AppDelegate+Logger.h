//
//  AppDelegate+Logger.h
//  iSignin
//
//  Created by Damon Yuan on 28/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "AppDelegate.h"

#define ISLOG_FLAG_FATAL    (1 << 0)
#define ISLOG_FLAG_ERROR    (1 << 1)
#define ISLOG_FLAG_WARN     (1 << 2)
#define ISLOG_FLAG_NOTICE   (1 << 3)
#define ISLOG_FLAG_INFO     (1 << 4)
#define ISLOG_FLAG_DEBUG    (1 << 5)

#define ISLOG_LEVEL_FATAL    (ISLOG_FLAG_FATAL)
#define ISLOG_LEVEL_ERROR    (ISLOG_FLAG_ERROR | ISLOG_FLAG_FATAL)
#define ISLOG_LEVEL_WARN     (ISLOG_FLAG_WARN | ISLOG_FLAG_ERROR | ISLOG_FLAG_FATAL)
#define ISLOG_LEVEL_NOTICE   (ISLOG_FLAG_NOTICE | ISLOG_FLAG_WARN | ISLOG_FLAG_ERROR | ISLOG_FLAG_FATAL)
#define ISLOG_LEVEL_INFO     (ISLOG_FLAG_INFO | ISLOG_FLAG_NOTICE | ISLOG_FLAG_WARN | ISLOG_FLAG_ERROR | ISLOG_FLAG_FATAL)
#define ISLOG_LEVEL_DEBUG    (ISLOG_FLAG_DEBUG | ISLOG_FLAG_INFO | ISLOG_FLAG_NOTICE | ISLOG_FLAG_WARN | ISLOG_FLAG_ERROR | ISLOG_FLAG_FATAL)

#define ISLogFatal(frmt, ...)      SYNC_LOG_OBJC_MAYBE(ISLogLevel, ISLOG_FLAG_FATAL,  0, frmt, ##__VA_ARGS__)
#define ISLogError(frmt, ...)      SYNC_LOG_OBJC_MAYBE(ISLogLevel, ISLOG_FLAG_ERROR,  0, frmt, ##__VA_ARGS__)
#define ISLogWarn(frmt, ...)      ASYNC_LOG_OBJC_MAYBE(ISLogLevel, ISLOG_FLAG_WARN,   0, frmt, ##__VA_ARGS__)
#define ISLogNotice(frmt, ...)    ASYNC_LOG_OBJC_MAYBE(ISLogLevel, ISLOG_FLAG_NOTICE, 0, frmt, ##__VA_ARGS__)
#define ISLogInfo(frmt, ...)      ASYNC_LOG_OBJC_MAYBE(ISLogLevel, ISLOG_FLAG_INFO,   0, frmt, ##__VA_ARGS__)
#define ISLogDebug(frmt, ...)     ASYNC_LOG_OBJC_MAYBE(ISLogLevel, ISLOG_FLAG_DEBUG,  0, frmt, ##__VA_ARGS__)

#define ISLogCFatal(frmt, ...)      SYNC_LOG_C_MAYBE(ISLogLevel, ISLOG_FLAG_FATAL,  0, frmt, ##__VA_ARGS__)
#define ISLogCError(frmt, ...)      SYNC_LOG_C_MAYBE(ISLogLevel, ISLOG_FLAG_ERROR,  0, frmt, ##__VA_ARGS__)
#define ISLogCWarn(frmt, ...)      ASYNC_LOG_C_MAYBE(ISLogLevel, ISLOG_FLAG_WARN,   0, frmt, ##__VA_ARGS__)
#define ISLogCNotice(frmt, ...)    ASYNC_LOG_C_MAYBE(ISLogLevel, ISLOG_FLAG_NOTICE, 0, frmt, ##__VA_ARGS__)
#define ISLogCInfo(frmt, ...)      ASYNC_LOG_C_MAYBE(ISLogLevel, ISLOG_FLAG_INFO,   0, frmt, ##__VA_ARGS__)
#define ISLogCDebug(frmt, ...)     ASYNC_LOG_C_MAYBE(ISLogLevel, ISLOG_FLAG_DEBUG,  0, frmt, ##__VA_ARGS__)

#define ISLogFatalWithDictionary(dic)   ISLogFatal(@"%@", [dic JSONDictionaryToString])
#define ISLogErrorWithDictionary(dic)   ISLogError(@"%@", [dic JSONDictionaryToString])
#define ISLogWarnWithDictionary(dic)    ISLogWarn(@"%@", [dic JSONDictionaryToString])
#define ISLogNoticeWithDictionary(dic)  ISLogNotice(@"%@", [dic JSONDictionaryToString])
#define ISLogInfoWithDictionary(dic)    ISLogInfo(@"%@", [dic JSONDictionaryToString])
#define ISLogDebugWithDictionary(dic)   ISLogDebug(@"%@", [dic JSONDictionaryToString])

#ifdef DEBUG
static const int ISLogLevel = ISLOG_LEVEL_DEBUG;
#else
static const int ISLogLevel = ISLOG_LEVEL_NOTICE;
#endif

@interface AppDelegate (Logger)

- (void)setupLogger;

@end
