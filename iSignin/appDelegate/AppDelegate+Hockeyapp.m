//
//  AppDelegate+Hockeyapp.m
//  iSignin
//
//  Created by Damon Yuan on 8/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "AppDelegate+Hockeyapp.h"

@implementation AppDelegate (Hockeyapp)

- (void)setupHockeyapp
{
    if (! [NSString isNilOrEmpty:HockeyappBetaID]) {
        [[BITHockeyManager sharedHockeyManager] configureWithBetaIdentifier:HockeyappBetaID
                                                             liveIdentifier:HockeyappAppLiveID
                                                                   delegate:self];
        [[BITHockeyManager sharedHockeyManager] startManager];
        [[BITHockeyManager sharedHockeyManager].crashManager setCrashManagerStatus:BITCrashManagerStatusAutoSend];
        [BITHockeyManager sharedHockeyManager].feedbackManager.requireUserEmail = BITFeedbackUserDataElementDontShow;
        [[BITHockeyManager sharedHockeyManager].authenticator authenticateInstallation];
    }
}

- (void)delayCheckCrashedInLastSession
{
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if ([BITHockeyManager sharedHockeyManager].crashManager.didCrashInLastSession) {
            [ISAppStyleSheet showAlertWithTitle:NSLocalizedString(@"Alert", @"AppDelegate+Hockeyapp")
                                        message:NSLocalizedString(@"The application iSignin quit unexpectedly. Would you like to send us a feedback?", @"AppDelegate+Hockeyapp")
                               otherButtonTitle:NSLocalizedString(@"Cancel", @"AppDelegate+Hockeyapp")];
        }
    });
}

#pragma mark - BITUpdateManagerDelegate
- (NSString *)customDeviceIdentifierForUpdateManager:(BITUpdateManager *)updateManager
{
#ifndef CONFIGURATION_AppStore
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    if ([[UIDevice currentDevice] respondsToSelector:@selector(uniqueIdentifier)])
        return [[UIDevice currentDevice] performSelector:@selector(uniqueIdentifier)];
#pragma clang diagnostic pop
#endif
    return nil;
}

- (NSString *)getLogFilesContentWithMaxSize:(NSInteger)maxSize {
    NSMutableString *description = [NSMutableString string];
    
    NSArray *sortedLogFileInfos = [[self.fileLogger logFileManager] sortedLogFileInfos];
    NSInteger count = [sortedLogFileInfos count];
    
    for (NSInteger index = count - 1; index >= 0; index--) {
        DDLogFileInfo *logFileInfo = [sortedLogFileInfos objectAtIndex:index];
        
        NSData *logData = [[NSFileManager defaultManager] contentsAtPath:[logFileInfo filePath]];
        if ([logData length] > 0) {
            NSString *result = [[NSString alloc] initWithBytes:[logData bytes]
                                                        length:[logData length]
                                                      encoding: NSUTF8StringEncoding];
            
            [description appendString:result];
        }
    }
    
    if ([description length] > maxSize) {
        description = (NSMutableString *)[description substringWithRange:NSMakeRange([description length]-maxSize-1, maxSize)];
    }
    
    return description;
}

#pragma mark - BITCrashManagerDelegate

- (NSString *)applicationLogForCrashManager:(BITCrashManager *)crashManager {
    NSString *description = [self getLogFilesContentWithMaxSize:5000];
    if ([description length] == 0) {
        return nil;
    } else {
        return description;
    }
}

- (void)crashManagerWillSendCrashReport:(BITCrashManager *)crashManager {
    ISLogDebug(@"Latest crash info will be sent.");
}

- (void)crashManager:(BITCrashManager *)crashManager didFailWithError:(NSError *)error {
    ISLogDebug(@"Latest crash info sent failed. Error: %@", [error description]);
}

- (void)crashManagerDidFinishSendingCrashReport:(BITCrashManager *)crashManager {
    ISLogDebug(@"Latest crash info has been sent.");
}

#pragma mark - BITHockeyManagerDelegate
- (NSString *)userIDForHockeyManager:(BITHockeyManager *)hockeyManager componentManager:(BITHockeyBaseManager *)componentManager {
    if (componentManager == hockeyManager.crashManager || componentManager == hockeyManager.feedbackManager) {
        // TODO
    }
    return nil;
}

- (NSString *)userNameForHockeyManager:(BITHockeyManager *)hockeyManager componentManager:(BITHockeyBaseManager *)componentManager {
    if (componentManager == hockeyManager.crashManager || componentManager == hockeyManager.feedbackManager) {
        return [ISUserDefaults currentUserAccount];
    }
    return nil;
}

@end
