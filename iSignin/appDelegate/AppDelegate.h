//
//  AppDelegate.h
//  iSignin
//
//  Created by Damon Yuan on 15/2/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>
#import "DDFileLogger.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, readonly) CLLocation *currentLocation;
@property (nonatomic, strong) DDFileLogger *fileLogger;

+ (AppDelegate *)sharedAppDelegate;

@end

