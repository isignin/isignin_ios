//
//  AppDelegate+Logger.m
//  iSignin
//
//  Created by Damon Yuan on 28/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "AppDelegate+Logger.h"
#import "DDTTYLogger.h"

@implementation AppDelegate (Logger)

- (void)setupLogger
{
    
#ifdef DEBUG
    [DDLog addLogger:[DDTTYLogger sharedInstance]
        withLogLevel:ISLOG_LEVEL_DEBUG];
#endif
    
}

@end
