//
//  Created by Allen Wei on 2/16/13.
//  Copyright 2011 Bindo Labs. All rights reserved.
//


#import "UIImage+JoinImages.h"

@implementation UIImage (JoinImages)
+ (UIImage *)imageByImages:(NSArray *)images {
    return [self imageByImages:images allowTransparency:YES];
}

+ (UIImage *)imageByImages:(NSArray *)images allowTransparency:(BOOL)allowTransparency {
    float maxWidth = 0;
    float maxHeight = 0;
    for (UIImage *im in images) {
        if (im.size.width > maxWidth) {
            maxWidth = im.size.width;
        }
        maxHeight += im.size.height;
    }

    CGSize newSize = CGSizeMake(maxWidth, maxHeight);
    UIGraphicsBeginImageContext(newSize);
    
    if (!allowTransparency) {
        [[UIColor whiteColor] setFill];
        UIRectFill(CGRectMake(0, 0, newSize.width, newSize.height));
    }
    
    float height = 0;
    for (UIImage *im in images) {
        [im drawInRect:CGRectMake(0, height, im.size.width, im.size.height)];
        height += im.size.height;
    }
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return finalImage;
}
@end