//
//  NSString+IntString.m
//  POS
//
//  Created by Ben Liong on 18/2/15.
//  Copyright (c) 2015 BindoLabs. All rights reserved.
//

#import "NSString+IntString.h"

@implementation NSString (IntString)

- (int)intForDigitAt:(NSUInteger)index {
    unichar ch = [self characterAtIndex:index];
    if (ch >= '0' && ch <= '9') {
        return ch - '0';
    } else
        return 0;
}
@end
