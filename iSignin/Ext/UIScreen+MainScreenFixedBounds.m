//
//  UIScreen+MainScreenFixedBounds.m
//  POS
//
//  Created by Damon on 5/10/14.
//  Copyright (c) 2014 BindoLabs. All rights reserved.
//

#import "UIScreen+MainScreenFixedBounds.h"

@implementation UIScreen (screenFixedBounds)

+ (CGRect)screenFixedBounds:(UIScreen *)screen
{
    // In iOS8 the bounds of main screen is interface-dependant.
    // This catagory is used to fix the bounds to Portrait Orientation.
    if ([screen respondsToSelector:@selector(fixedCoordinateSpace)]) {
        // iOS8 SDK
        return [screen.coordinateSpace convertRect:screen.bounds toCoordinateSpace:screen.fixedCoordinateSpace];
    }
    return screen.bounds;
}

+ (CGRect)mainScreenFixedBounds
{
    return [self screenFixedBounds:[UIScreen mainScreen]];
}

@end
