//
//  RegexHelper.h
//  Bindo POS
//
//  Created by Allen on 3/29/12.
//  Copyright (c) 2012 Bindo Labs Inc.All rights reserved.
//

#import <Foundation/Foundation.h>

extern const NSString *RegexPattermEmail;

@interface RegexHelper : NSObject
+ (BOOL)isValidEmail: (NSString *)email;
+ (BOOL)isValidUsername: (NSString *)username;
+ (BOOL)isValidUSZipcode:(NSString *)USZipcode;
+ (BOOL)isValidCAZipcode:(NSString *)CAZipcode;
@end
