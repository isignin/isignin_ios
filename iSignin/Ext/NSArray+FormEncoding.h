//
//  NSArray+FormEncoding.h
//  Bindo POS
//
//  Created by Allen on 12/4/13.
//  Copyright (c) 2013 BindoLabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (FormEncoding)
- (NSString *)formEncodedString;
- (NSData *)formEncodedData;
@end
