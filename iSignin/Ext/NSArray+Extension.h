//
//  NSArray+BPExtension.h
//  Bindo POS
//
//  Created by Kjuly on 3/7/14.
//  Copyright (c) 2014 BindoLabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Extension)

/**
 * Calls performSelector on all objects that can receive the selector in the array.
 * Makes an iterable copy of the array, making it possible for the selector to modify
 * the array. Contrast this with makeObjectsPerformSelector which does not allow side effects of
 * modifying the array.
 */
- (void)perform:(SEL)selector;
- (void)perform:(SEL)selector withObject:(id)p1;
- (void)perform:(SEL)selector withObject:(id)p1 withObject:(id)p2;

@end
