//
//  NSString+EscapeParameter.h
//  Bindo POS
//
//  Created by Allen on 12/24/13.
//  Copyright (c) 2013 BindoLabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (EscapeParameter)

// Convert to a valid SQL string
- (NSString *)toValidSqlString;
// LIKE SELF
- (NSString *)toLikeQueryForAttribute:(NSString *)attribute;
// SELF is alreay valid, no need further conversion
- (NSString *)toLikeQueryFromValidSqlStringForAttribute:(NSString *)attribute;
- (NSString *)toLikeQueryFromValidSqlStringForPhoneAttribute:(NSString *)phoneAttribute;
// Split SELF into parts and LIKE all parts (for name searching)
- (NSString *)toDepthMatchingLikeQueryForAttribute:(NSString *)attribute;

#pragma mark - Like queries for data models
- (NSString *)toLikeQueryForListing;
- (NSString *)toLikeQueryForSupplier;
- (NSString *)toLikeQueryForCustomer;
- (NSString *)toLikeQueryForCustomerCreditCard;
- (NSString *)toLikeQueryForDepartment;
- (NSString *)toLikeQueryForDiscount;

@end
