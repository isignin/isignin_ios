//
//  NSData+JSONDictionary.m
//  iSignin
//
//  Created by Damon Yuan on 22/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "NSData+JSONDictionary.h"

@implementation NSData (JSONDictionary)

- (NSDictionary *)JSONDataToDictionary
{
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:self
                                                         options:NSJSONReadingMutableContainers
                                                           error:&error];
    return (error ? nil : json);
}

@end
