//
//  UIViewController+Container.h
//  Bindo POS
//
//  Created by Levey on 4/4/14.
//  Copyright (c) 2014 BindoLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Container)

- (void)addAndDisplayChildViewController:(UIViewController *)childViewController inView:(UIView *)view;
- (void)addAndDisplayChildViewController:(UIViewController *)childViewController withFrame:(CGRect)frame inView:(UIView *)view;
- (void)addAndDisplayChildViewController:(UIViewController *)childViewController;
- (void)addAndDisplayChildViewController:(UIViewController *)childViewController withFrame:(CGRect)frame;
- (void)removeChildViewControllerFromSelf:(UIViewController *)childViewController;
- (void)removeSelfFromParentViewController;

@end
