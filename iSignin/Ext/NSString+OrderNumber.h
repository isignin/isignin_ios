//
//  Created by Allen Wei on 2/21/13.
//  Copyright 2011 Bindo Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (OrderNumber)

- (NSString *)stringToOrderNumber;
- (NSString *)stringToInvoiceNumber;

@end
