//
//  NSString+JSONDictionary.m
//  iSignin
//
//  Created by Damon Yuan on 22/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "NSString+JSONDictionary.h"

@implementation NSString (JSONDictionary)

- (NSDictionary *)JSONStringToDictionary
{
    NSError *error;
    NSData *objectData = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                         options:NSJSONReadingMutableContainers
                                                           error:&error];
    return (error ? nil : json);
}

@end
