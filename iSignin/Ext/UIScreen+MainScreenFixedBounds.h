//
//  UIScreen+MainScreenFixedBounds.h
//  POS
//
//  Created by Damon on 5/10/14.
//  Copyright (c) 2014 BindoLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScreen (screenFixedBounds)
+ (CGRect)screenFixedBounds:(UIScreen *)screen;
+ (CGRect)mainScreenFixedBounds;
@end
