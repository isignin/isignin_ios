//
//  NSString+QueryParameters.m
//  Bindo POS
//
//  Created by Allen on 10/10/13.
//  Copyright (c) 2013 BindoLabs. All rights reserved.
//

#import "NSString+QueryParameters.h"


NSString *encodeURIComponent(NSString *string) {
    return (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                                 NULL,
                                                                                 (__bridge CFStringRef)string,
                                                                                 NULL,
                                                                                 CFSTR("!*'();:@&=+$,/?%#[]"),
                                                                                 CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
    
}

@implementation NSString (QueryParameters)

- (NSString *)queryStringFromDictionary:(NSDictionary *)dictionary {
    NSMutableArray *query = [NSMutableArray array];
    for (NSString *key in [dictionary allKeys]) {
        id value = [dictionary objectForKey:key];
        [query addObject:[NSString stringWithFormat:@"%@=%@",
                          encodeURIComponent([key description]),
                          encodeURIComponent([value description])]];
    }
    return [query componentsJoinedByString:@"&"];
}

- (NSString *)stringByAppendingQueryParameters:(NSDictionary *)parameters {
    return [self stringByAppendingFormat:[self rangeOfString:@"?"].location == NSNotFound ? @"?%@" : @"&%@", [self queryStringFromDictionary:parameters]];
}



@end
