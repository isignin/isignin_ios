//
//  NSArray+BPExtension.m
//  Bindo POS
//
//  Created by Kjuly on 3/7/14.
//  Copyright (c) 2014 BindoLabs. All rights reserved.
//

#import "NSArray+Extension.h"

@implementation NSArray (Extension)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
- (void)perform:(SEL)selector
{
    NSArray *copy = [[NSArray alloc] initWithArray:self];
    NSEnumerator* e = [copy objectEnumerator];
    for (id delegate; (delegate = [e nextObject]); ) {
        if ([delegate respondsToSelector:selector]) {
            [delegate performSelector:selector];
        }
    }
}

- (void)perform:(SEL)selector withObject:(id)p1
{
    NSArray *copy = [[NSArray alloc] initWithArray:self];
    NSEnumerator* e = [copy objectEnumerator];
    for (id delegate; (delegate = [e nextObject]); ) {
        if ([delegate respondsToSelector:selector]) {
            [delegate performSelector:selector withObject:p1];
        }
    }
}

- (void)perform:(SEL)selector withObject:(id)p1 withObject:(id)p2
{
    NSArray *copy = [[NSArray alloc] initWithArray:self];
    NSEnumerator* e = [copy objectEnumerator];
    for (id delegate; (delegate = [e nextObject]); ) {
        if ([delegate respondsToSelector:selector]) {
            [delegate performSelector:selector withObject:p1 withObject:p2];
        }
    }
}
#pragma clang diagnostic pop


@end
