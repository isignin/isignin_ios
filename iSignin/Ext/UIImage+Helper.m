//
//  UIImage+BPHelper.m
//  Bindo POS
//
//  Created by Kjuly on 19/2/14.
//  Copyright (c) 2014 BindoLabs. All rights reserved.
//

#import "UIImage+Helper.h"

static inline double radians (double degrees) { return degrees * M_PI/180; }

@implementation UIImage (Helper)

+ (UIImage *)bp_resizedProductImage:(UIImage *)image
{
    float targetSize = 2048.f;
    UIImage *newImage;
    
    if (image.size.width > image.size.height) {
        float oldWidth = image.size.width;
        if (oldWidth > targetSize) {
            float scaleFactor = targetSize / oldWidth;
            
            float newHeight = image.size.height * scaleFactor;
            float newWidth = oldWidth * scaleFactor;
            
            UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
            [image drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
            newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        } else {
            newImage = image;
        }
    } else {
        float oldHeight = image.size.height;
        if (oldHeight > targetSize) {
            float scaleFactor = targetSize / oldHeight;
            
            float newWidth = image.size.width * scaleFactor;
            float newHeight = oldHeight * scaleFactor;
            
            UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
            [image drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
            newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        } else {
            newImage = image;
        }
    }
    return newImage;
}

+ (UIImage *)bp_resizedAvatarImage:(UIImage *)avatarImage
{
    CGSize targetSize = CGSizeMake(195.f, 195.f);
    
    float ratio, delta;
    CGPoint offset;
    
    // Figure out if the picture is landscape or portrait,
    //   then calculate scale factor and offset
    if (avatarImage.size.width > avatarImage.size.height) {
        ratio  = targetSize.width / avatarImage.size.width;
        delta  = (ratio * avatarImage.size.width - ratio * avatarImage.size.height);
        offset = CGPointMake(delta / 2.f, 0.f);
    } else {
        ratio  = targetSize.width / avatarImage.size.height;
        delta  = (ratio * avatarImage.size.height - ratio * avatarImage.size.width);
        offset = CGPointMake(0.f, delta / 2.f);
    }
    
    // Make the final clipping rect based on the calculated values
    CGRect clipRect = CGRectMake(-offset.x, -offset.y,
                                 (ratio * avatarImage.size.width) + delta,
                                 (ratio * avatarImage.size.height) + delta);
    
    
    // Start a new context, with scale factor 0.f,
    //   so retina displays get high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(targetSize, YES, 0.f);
    } else {
        UIGraphicsBeginImageContext(targetSize);
    }
    UIRectClip(clipRect);
    [avatarImage drawInRect:clipRect];
    UIImage * resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resultImage;
}

+ (UIImage *)bp_rotate:(UIImage *)initImage
{
    return [self bp_rotate:initImage withOrientation:initImage.imageOrientation];
}

+ (UIImage *)bp_rotate:(UIImage *)initImage
       withOrientation:(UIImageOrientation)orientation
{
    return         [self bp_rotate:initImage
                   withOrientation:orientation
viewControllerInterfaceOrientation:UIInterfaceOrientationLandscapeLeft];
}

+ (UIImage *)            bp_rotate:(UIImage *)initImage
                   withOrientation:(UIImageOrientation)orientation
viewControllerInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;
{
    UIImage * sourceImage = initImage;
    if (sourceImage.imageOrientation == UIImageOrientationUp) {
        return sourceImage;
    }
    
    CGFloat targetWidth, targetHeight;
    if (IS_PAD || UIInterfaceOrientationIsLandscape(interfaceOrientation) == NO) {
        targetWidth = initImage.size.width > initImage.size.height? initImage.size.width: initImage.size.height;
        targetHeight = initImage.size.width > initImage.size.height? initImage.size.height: initImage.size.width;
    }
    else {
        targetWidth = initImage.size.width > initImage.size.height? initImage.size.height: initImage.size.width;
        targetHeight = initImage.size.width > initImage.size.height? initImage.size.width: initImage.size.height;
    }
    
    CGImageRef imageRef = [sourceImage CGImage];
    CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(imageRef);
    CGColorSpaceRef colorSpaceInfo = CGImageGetColorSpace(imageRef);
    
//    if (bitmapInfo == kCGImageAlphaNone) {
//        bitmapInfo = (CGBitmapInfo) kCGImageAlphaNoneSkipLast;
//    }
    
    CGContextRef bitmap;
    
    if (sourceImage.imageOrientation == UIImageOrientationUp || sourceImage.imageOrientation == UIImageOrientationDown) {
        bitmap = CGBitmapContextCreate(NULL, targetWidth, targetHeight, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, bitmapInfo);
        
    } else {
        bitmap = CGBitmapContextCreate(NULL, targetHeight, targetWidth, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, bitmapInfo);
        
    }
    
    if (sourceImage.imageOrientation == UIImageOrientationLeft) {
        CGContextRotateCTM (bitmap, radians(90));
        CGContextTranslateCTM (bitmap, 0, -targetHeight);
        
    } else if (sourceImage.imageOrientation == UIImageOrientationRight) {
        CGContextRotateCTM (bitmap, radians(-90));
        CGContextTranslateCTM (bitmap, -targetWidth, 0);
        
    } else if (sourceImage.imageOrientation == UIImageOrientationUp) {
        // NOTHING
    } else if (sourceImage.imageOrientation == UIImageOrientationDown) {
        CGContextTranslateCTM (bitmap, targetWidth, targetHeight);
        CGContextRotateCTM (bitmap, radians(-180.));
    }
    
    CGContextDrawImage(bitmap, CGRectMake(0, 0, targetWidth, targetHeight), imageRef);
    CGImageRef ref = CGBitmapContextCreateImage(bitmap);
    UIImage* newImage = [UIImage imageWithCGImage:ref];
    
    CGContextRelease(bitmap);
    CGImageRelease(ref);
    
    return newImage;
}

+ (UIImage *)bp_resizeImage:(UIImage*)image toSize:(CGSize)newSize {
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGImageRef imageRef = image.CGImage;
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height);
    
    CGContextConcatCTM(context, flipVertical);
    // Draw into the context; this scales the image
    CGContextDrawImage(context, newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    CGImageRelease(newImageRef);
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (UIImage *)bp_resizeToSize:(CGSize)newSize {
    return [UIImage bp_resizeImage:self toSize:newSize];
}

@end
