//
//  UIImage+Resizing.h
//  Bindo POS
//
//  Created by Allen on 7/11/13.
//  Copyright (c) 2013 BindoLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Resizing)
+ (UIImage *)imageWithImage:(UIImage *)image scaledToWidth: (float)newWidth;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToFitSize:(CGSize)size;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToWidth:(float)newWidth toHeight:(float)newHeight;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToWidth: (float)newWidth marginX: (float)marginX;

- (UIImage *)fitInSize:(CGSize)newSize;
@end
