//
//  UIImage+BPExtentation.h
//  Bindo POS
//
//  Created by Kjuly on 10/17/13.
//  Copyright (c) 2013 BindoLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extentation)

- (UIImage *)tintWithColor:(UIColor *)tintColor;

@end
