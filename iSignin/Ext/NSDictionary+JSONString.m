//
//  NSDictionary+JSONString.m
//  iSignin
//
//  Created by Damon Yuan on 23/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "NSDictionary+JSONString.h"

@implementation NSDictionary (JSONString)

- (NSString *)JSONDictionaryToString
{
    NSData *jsonData = [self JSONDictionaryToData];
    if (! jsonData) {
        return nil;
    }
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

@end
