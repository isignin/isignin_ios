//
//  RegexHelper.m
//  Bindo POS
//
//  Created by Allen on 3/29/12.
//  Copyright (c) 2012 Bindo Labs Inc.All rights reserved.
//

#import "RegexHelper.h"
#import "RegexKitLite.h"

NSString *RegexPattermEmail = @"([a-zA-Z0-9%_.+\\-]+)@([a-zA-Z0-9.\\-]+?\\.[a-zA-Z]{2,6})";
NSString *RegexPattermUSZipcode = @"(^[0-9]{5}(-[0-9]{4})?$)";
NSString *RegexPattermCAZipcode = @"(^[a-zA-Z][0-9][a-zA-Z][- ]*[0-9][a-zA-Z][0-9]$)";

@implementation RegexHelper
+ (BOOL)isValidEmail:(NSString *)email {
    return [email isMatchedByRegex:RegexPattermEmail];
}

+ (BOOL)isValidUsername:(NSString *)username {
    return [username isMatchedByRegex:@"^[a-zA-Z0-9\\d_]+$"];
}

+ (BOOL)isValidUSZipcode:(NSString *)USZipcode {
    return [USZipcode isMatchedByRegex:RegexPattermUSZipcode];
}

+ (BOOL)isValidCAZipcode:(NSString *)CAZipcode {
    return [CAZipcode isMatchedByRegex:RegexPattermCAZipcode];
}
@end
