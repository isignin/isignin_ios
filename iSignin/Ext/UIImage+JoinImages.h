//
//  Created by Allen Wei on 2/16/13.
//  Copyright 2011 Bindo Labs. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface UIImage (JoinImages)
+(UIImage *)imageByImages: (NSArray *)images;
+(UIImage *)imageByImages: (NSArray *)images allowTransparency: (BOOL)allowTransparency;

@end