//
//  Bindo POS
//
//  Created by Allen on 3/13/12.
//  Copyright (c) 2012 Bindo Labs Inc.All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ModalAdditions)

- (BOOL)isModal;

@end
