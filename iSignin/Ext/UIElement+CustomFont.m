//
//  UILabel+CustomFont.m
//  Bindo POS
//
//  Created by Allen on 11/3/13.
//  Copyright (c) 2013 BindoLabs. All rights reserved.
//

#import "UIElement+CustomFont.h"
#import "RegexKitLite.h"

@implementation UILabel (CustomFont)
-(void)awakeFromNib {
    [super awakeFromNib];
    if ([self.font.fontName isMatchedByRegex:@"(Bold|MediumP4)"]) {
        self.font = [UIFont boldSystemFontOfSize:self.font.pointSize];
    } else {
        self.font = [UIFont systemFontOfSize:self.font.pointSize];
    }
}
@end

@implementation UITextField (CustomFont)
-(void)awakeFromNib {
    [super awakeFromNib];
    if ([self.font.fontName isMatchedByRegex:@"(Bold|MediumP4)"]) {
        self.font = [UIFont boldSystemFontOfSize:self.font.pointSize];
    } else {
        self.font = [UIFont systemFontOfSize:self.font.pointSize];
    }
}
@end

@implementation UITextView (CustomFont)
-(void)awakeFromNib {
    [super awakeFromNib];
    if ([self.font.fontName isMatchedByRegex:@"(Bold|MediumP4)"]) {
        self.font = [UIFont boldSystemFontOfSize:self.font.pointSize];
    } else {
        self.font = [UIFont systemFontOfSize:self.font.pointSize];
    }
}
@end