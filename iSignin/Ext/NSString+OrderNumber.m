//
//  Created by Allen Wei on 2/21/13.
//  Copyright 2011 Bindo Labs. All rights reserved.
//

#import "NSString+OrderNumber.h"

@implementation NSString (OrderNumber)

- (NSString *)stringToOrderNumber
{
    //2011-1009-2233385-383
    //2011-1013-0743401-669
    if (self.length == 18) {
        NSString *number = [self copy];
        NSArray *parts = [NSArray arrayWithObjects:
                [number substringWithRange:NSMakeRange(0, 4)],
                [number substringWithRange:NSMakeRange(4, 4)],
                [number substringWithRange:NSMakeRange(8, 7)],
                [number substringWithRange:NSMakeRange(15, 3)],
                nil];
        return [parts componentsJoinedByString:@"-"];
    } else {
        return self;
    }
}

- (NSString *)stringToInvoiceNumber
{
    // 2014-1234-1234567-123-010100
    // 2014-1234-1234567-123-Aaaaaaaaa
    if (self.length == 24) {
        NSString *number = [self copy];
        NSArray *parts = @[[number substringWithRange:NSMakeRange(0, 4)],
                           [number substringWithRange:NSMakeRange(4, 4)],
                           [number substringWithRange:NSMakeRange(8, 7)],
                           [number substringWithRange:NSMakeRange(15, 3)],
                           [number substringWithRange:NSMakeRange(18, 6)]];
        return [parts componentsJoinedByString:@"-"];
    }
    else if (self.length >= 18) {
        NSString *number = [self copy];
        NSArray *parts = @[[number substringWithRange:NSMakeRange(0, 4)],
                           [number substringWithRange:NSMakeRange(4, 4)],
                           [number substringWithRange:NSMakeRange(8, 7)],
                           [number substringWithRange:NSMakeRange(15, 3)]];
        if (self.length == 18) {
            return [parts componentsJoinedByString:@"-"];
        } else {
            return [[parts componentsJoinedByString:@"-"] stringByAppendingString:
                    [NSString stringWithFormat:@"-%@", [number substringWithRange:NSMakeRange(18, self.length - 18)]]];
        }
    }
    else {
        return self;
    }
}

@end
