//
//  NSDictionary+JSONData.h
//  iSignin
//
//  Created by Damon Yuan on 23/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (JSONData)

- (NSData *)JSONDictionaryToData;

@end
