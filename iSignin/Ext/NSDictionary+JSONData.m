//
//  NSDictionary+JSONData.m
//  iSignin
//
//  Created by Damon Yuan on 23/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "NSDictionary+JSONData.h"

@implementation NSDictionary (JSONData)

- (NSData *)JSONDictionaryToData
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    if (error || !jsonData) {
        return nil;
    }
    return jsonData;
}

@end
