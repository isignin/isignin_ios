//
//  NSString+QueryParameters.h
//  Bindo POS
//
//  Created by Allen on 10/10/13.
//  Copyright (c) 2013 BindoLabs. All rights reserved.
//

#import <Foundation/Foundation.h>
NSString *encodeURIComponent(NSString *string);
@interface NSString (QueryParameters)
- (NSString *)queryStringFromDictionary:(NSDictionary *)dictionary;
- (NSString *)stringByAppendingQueryParameters:(NSDictionary *)parameters;
@end
