//
//  UIWindow+OrientationHelper.m
//  Bindo POS
//
//  Created by Allen on 5/8/13.
//  Copyright (c) 2013 BindoLabs. All rights reserved.
//

#import "UIWindow+OrientationHelper.h"

@implementation UIWindow (OrientationHelper)
- (CGAffineTransform)transformForCurrentOrientation {
	UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
	float radians = 0;
    if (IS_PAD) {
        if (UIInterfaceOrientationIsLandscape(orientation)) {
            if (orientation == UIInterfaceOrientationLandscapeLeft) {
                radians = -M_PI_2;
            } else {
                radians = M_PI_2;
            }
        }
    }
    return CGAffineTransformMakeRotation(radians);
}
@end
