//
//  NSPointerArray+BPExtension.h
//  POS
//
//  Created by Levey on 12/28/14.
//  Copyright (c) 2014 BindoLabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSPointerArray (Extension)

- (void)addObject:(id)object;
- (void)removeObject:(id)object;
- (BOOL)containsObject:(id)object;
- (void)makeObjectsPerformSelector:(SEL)aSelector withObject:(id)object;

@end
