//
//  UIViewController+ModalAdditions.m
//  Bindo POS
//
//  Created by Allen on 3/13/12.
//  Copyright (c) 2012 Bindo Labs Inc.All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>


@implementation UIViewController (ModalAdditions)

- (BOOL)isModal
{
    BOOL isModal = ((self.parentViewController && self.parentViewController.presentedViewController == self) ||
            //or if I have a navigation controller, check if its parent modal view controller is self navigation controller
            (self.navigationController && self.navigationController.parentViewController && self.navigationController.parentViewController.presentedViewController == self.navigationController) ||
            //or if the parent of my UITabBarController is also a UITabBarController class, then there is no way to do that, except by using a modal presentation
            [[[self tabBarController] parentViewController] isKindOfClass:[UITabBarController class]]);
    
    //iOS 5+
    if (!isModal && [self respondsToSelector:@selector(presentingViewController)]) {
        isModal = ((self.presentingViewController && self.presentingViewController.presentedViewController == self) ||
                //or if I have a navigation controller, check if its parent modal view controller is self navigation controller
                (self.navigationController && self.navigationController.presentingViewController && self.navigationController.presentingViewController.presentedViewController == self.navigationController) ||
                //or if the parent of my UITabBarController is also a UITabBarController class, then there is no way to do that, except by using a modal presentation
                [[[self tabBarController] presentingViewController] isKindOfClass:[UITabBarController class]]);
    }
    
    return isModal;
}

@end
