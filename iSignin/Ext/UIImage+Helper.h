//
//  UIImage+BPHelper.h
//  Bindo POS
//
//  Created by Kjuly on 19/2/14.
//  Copyright (c) 2014 BindoLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Helper)

+ (UIImage *)bp_resizedProductImage:(UIImage *)image;
+ (UIImage *)bp_resizedAvatarImage:(UIImage *)avatarImage;

+ (UIImage *)bp_rotate:(UIImage *)initImage;
+ (UIImage *)bp_rotate:(UIImage *)initImage
       withOrientation:(UIImageOrientation)orientation;

+ (UIImage *)            bp_rotate:(UIImage *)initImage
                   withOrientation:(UIImageOrientation)orientation
viewControllerInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;
+ (UIImage *)bp_resizeImage:(UIImage*)image toSize:(CGSize)newSize;
- (UIImage *)bp_resizeToSize:(CGSize)newSize;

@end
