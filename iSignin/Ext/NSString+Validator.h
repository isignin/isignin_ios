//
//  NSString+Validator.h
//  Bindo POS
//
//  Created by Levey on 5/15/14.
//  Copyright (c) 2014 BindoLabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Validator)

+ (BOOL)isNilOrEmpty:(NSString *)input;
- (BOOL)isEmail;
- (BOOL)isPhoneNumber;
- (BOOL)isDigit;
- (BOOL)isNumeric;
- (BOOL)isAlphanumeric;
- (BOOL)hasBothCases;
- (BOOL)isUrl;
- (BOOL)isMinLength:(NSUInteger)length;
- (BOOL)isMaxLength:(NSUInteger)length;
- (BOOL)isMinLength:(NSUInteger)min andMaxLength:(NSUInteger)max;
- (BOOL)isEmpty;

@end
