//
//  UITableView+BPExtension.h
//  Bindo POS
//
//  Created by Kjuly on 3/7/14.
//  Copyright (c) 2014 BindoLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Extension)

- (void)scrollToTop:(BOOL)animated;
- (void)scrollToBottom:(BOOL)animated;
- (void)scrollToFirstRow:(BOOL)animated;
- (void)scrollToLastRow:(BOOL)animated;

@end
