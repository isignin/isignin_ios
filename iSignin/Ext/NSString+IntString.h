//
//  NSString+IntString.h
//  POS
//
//  Created by Ben Liong on 18/2/15.
//  Copyright (c) 2015 BindoLabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (IntString)

- (int)intForDigitAt:(NSUInteger)index;

@end
