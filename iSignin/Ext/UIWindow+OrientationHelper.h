//
//  UIWindow+OrientationHelper.h
//  Bindo POS
//
//  Created by Allen on 5/8/13.
//  Copyright (c) 2013 BindoLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (OrientationHelper)
- (CGAffineTransform)transformForCurrentOrientation;
@end
