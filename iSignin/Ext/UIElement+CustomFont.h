//
//  UILabel+CustomFont.h
//  Bindo POS
//
//  Created by Allen on 11/3/13.
//  Copyright (c) 2013 BindoLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (CustomFont)

@end

@interface UITextField (CustomFont)

@end

@interface UITextView (CustomFont)

@end
