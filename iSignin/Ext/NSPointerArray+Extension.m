//
//  NSPointerArray+BPExtension.m
//  POS
//
//  Created by Levey on 12/28/14.
//  Copyright (c) 2014 BindoLabs. All rights reserved.
//

#import "NSPointerArray+Extension.h"

@implementation NSPointerArray (Extension)

#pragma mark - Private methods

- (NSUInteger)bp_indexOfPointer:(void *)pointer {
    NSUInteger i = 0;
    for (i = 0; i < [self count]; i++)
    {
        void *p = [self pointerAtIndex:i];
        if (p == pointer)
            return i;
    }
    return NSNotFound;
}

- (BOOL)containsPointer:(void *)pointer {
    return [self bp_indexOfPointer:pointer] != NSNotFound;
}

- (void)bp_removePointer:(void *)pointer {
    if (pointer == NULL) {
        return;
    }
    NSUInteger index = [self bp_indexOfPointer:pointer];
    if (index == NSNotFound)
        return;
    [self removePointerAtIndex:index];
}

#pragma mark - Instance methods

- (void)addObject:(id)object {
    [self addPointer:(__bridge void *)(object)];
}

- (void)removeObject:(id)object {
    [self bp_removePointer:(__bridge void *)(object)];
}

- (BOOL)containsObject:(id)object {
    return [self containsPointer:(__bridge void *)(object)];
}

- (void)makeObjectsPerformSelector:(SEL)aSelector withObject:(id)object {
    for (id pointer in [self allObjects]) {
        if ([pointer respondsToSelector:aSelector]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [pointer performSelector:aSelector withObject:object];
#pragma clang diagnostic pop
        }
    }
}

@end
