//
//  NSString+HTTPHelper.m
//  iSignin
//
//  Created by Damon Yuan on 7/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "NSString+HTTPHelper.h"

@implementation NSString (HTTPHelper)
+ (NSString *)URLEncodedString: (id)value {
    NSString *string = [value description];
    return (__bridge_transfer  NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef)string, (__bridge CFStringRef) @"[].", (__bridge CFStringRef)@":/?&=;+!@#$()',*", CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
}
@end
