//
//  NSData+HexNSString.h
//  Bindo POS
//
//  Copyright (c) 2014 BindoLabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (HexNSString)
-(NSString*)hexRepresentationWithSpaces_AS:(BOOL)spaces;
+ (NSData*)hexStringToData:(NSString*)string;
@end
