//
//  UIView+BPExtension.h
//  Bindo POS
//
//  Created by Kjuly on 3/7/14.
//  Copyright (c) 2014 BindoLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Extension)

- (UIViewController *)viewController;

@end
