//
//  NSString+NSPredicate.m
//  Bindo POS
//
//  Created by iwill on 2013-01-06.
//  Copyright (c) 2013年 BindoLabs. All rights reserved.
//

#import "NSString+NSPredicate.h"

@implementation NSString (NSPredicate)

- (NSString *)predicateExpressionString {
    NSMutableCharacterSet *characterSet = [[NSCharacterSet letterCharacterSet] mutableCopy];
    [characterSet formUnionWithCharacterSet:[NSCharacterSet decimalDigitCharacterSet]];
    [characterSet invert];
    
    NSMutableString *expression = [NSMutableString string];
    for (NSString *component in [self componentsSeparatedByCharactersInSet:characterSet]) {
        if (component.length) {
            [expression appendFormat:@"*%@", component];
        }
    }
    if (expression.length) {
        [expression appendString:@"*"];
    }
    return expression;
}

+ (NSString *)predicateExpressionStringFromKeyword:(NSString *)keyword {
    return [keyword predicateExpressionString];
}

@end
