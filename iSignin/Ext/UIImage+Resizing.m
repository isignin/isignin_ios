//
//  UIImage+Resizing.m
//  Bindo POS
//
//  Created by Allen on 7/11/13.
//  Copyright (c) 2013 BindoLabs. All rights reserved.
//

#import "UIImage+Resizing.h"

@implementation UIImage (Resizing)
+ (UIImage *)imageWithImage:(UIImage *)image scaledToWidth: (float)newWidth {
    return [UIImage imageWithImage:image scaledToWidth:newWidth marginX:0];
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToWidth:(float)newWidth toHeight:(float)newHeight {
    float width = newWidth;
    float height = newHeight;
    CGSize newSize = CGSizeMake(width, height);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToFitSize:(CGSize)size
{
    //calculate rect
    CGFloat aspect = image.size.width / image.size.height;
    if (size.width / aspect <= size.height)
    {
        CGSize aspectFitSize = CGSizeMake(size.width, size.width / aspect);
        //create drawing context
        UIGraphicsBeginImageContextWithOptions(aspectFitSize, NO, 0.0f);
        //draw
        [image drawInRect:CGRectMake(0.0f, 0.0f, aspectFitSize.width, aspectFitSize.height)];
        //capture resultant image
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        //return image
        return image;
    }
    else
    {
        CGSize aspectFitSize = CGSizeMake(size.height * aspect, size.height);
        //create drawing context
        UIGraphicsBeginImageContextWithOptions(aspectFitSize, NO, 0.0f);
        //draw
        [image drawInRect:CGRectMake(0.0f, 0.0f, aspectFitSize.width, aspectFitSize.height)];
        //capture resultant image
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        //return image
        return image;
    }
}


+ (UIImage *)imageWithImage:(UIImage *)image scaledToWidth: (float)width marginX: (float)marginX {
    float newWidth = width - marginX;
    float newHeight = image.size.height / (image.size.width / newWidth);
    CGSize newSize = CGSizeMake(newWidth, newHeight);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(marginX, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (UIImage *)fitInSize:(CGSize)newSize
{
    if (!newSize.height || !newSize.width)
    {
#if DEBUG
        [NSException raise:@"Invalid size." format:@"Size %f:%f is invalid.", newSize.width, newSize.height];
#endif
        return self;
    }
    
    const CGFloat widthRatio = newSize.width / self.size.width;
    const CGFloat heightRatio = newSize.height / self.size.height;
    const CGFloat ratio = MIN(widthRatio, heightRatio);
    
    const CGFloat imageWidth = self.size.width * ratio;
    const CGFloat imageHeight = self.size.height * ratio;
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(imageWidth, imageHeight), NO, 0.0);
    [self drawInRect:CGRectMake(0, 0, imageWidth, imageHeight)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resizedImage;
}

@end
