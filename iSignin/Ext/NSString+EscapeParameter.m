//
//  NSString+EscapeParameter.m
//  Bindo POS
//
//  Created by Allen on 12/24/13.
//  Copyright (c) 2013 BindoLabs. All rights reserved.
//

#import "NSString+EscapeParameter.h"

#import "RegexKitLite.h"


@implementation NSString (EscapeParameter)

- (NSString *)toValidSqlString
{
    NSString * validSqlString = [self stringByReplacingOccurrencesOfString:@"\"" withString:@"'"];
    return validSqlString;
}

- (NSString *)toLikeQueryForAttribute:(NSString *)attribute
{
    return [[self toValidSqlString] toLikeQueryFromValidSqlStringForAttribute:attribute];
}

- (NSString *)toLikeQueryFromValidSqlStringForAttribute:(NSString *)attribute
{
    if (self.length == 0) {
        return @"";
    }
    return [NSString stringWithFormat:@"%@ LIKE \"%%%@%%\"", attribute, self];
}

- (NSString *)toLikeQueryFromValidSqlStringForPhoneAttribute:(NSString *)phoneAttribute
{
    if (self.length == 0) {
        return @"";
    }
    
    //
    //   |extraQuery| is only for US phone number, which stored with format like (123) 456-7890.
    //   HK phone number is stored as raw data, it can just use original query.
    //
    // TODO:
    //
    //   We need to convert all phone numbers to raw data without any format (maybe need to
    //     include country code then), and after that, this |extraQuery| can be removed.
    //
    NSString * extraQuery = nil;
    if (self.length > 6) {
        // to match *123*456*7890
        //   e.g. (123) 456-7890
        extraQuery = [NSString stringWithFormat:@"%@ LIKE \"%%%@%%%@%%%@%%\"", phoneAttribute, [self substringToIndex:3],
                      [self substringWithRange:NSMakeRange(3, 3)],
                      [self substringFromIndex:6]];
    } else if (self.length > 3) {
        // to match *123*456
        //   e.g. (123) 456 or 123-456
        extraQuery = [NSString stringWithFormat:@"%@ LIKE \"%%%@%%%@%%\"", phoneAttribute, [self substringToIndex:3],
                      [self substringFromIndex:3]];
    }
    
    if (extraQuery) {
        return [NSString stringWithFormat:@"(%@ LIKE \"%%%@%%\") OR (%@)", phoneAttribute, self, extraQuery];
    } else {
        return [NSString stringWithFormat:@"%@ LIKE \"%%%@%%\"", phoneAttribute, self];
    }
}

- (NSString *)toDepthMatchingLikeQueryForAttribute:(NSString *)attribute
{
    if (self.length == 0) {
        return @"";
    }
    
    NSString * validSqlString = [self toValidSqlString];
    NSString * selfLikeQuery = [validSqlString toLikeQueryFromValidSqlStringForAttribute:attribute];
    
    NSArray * components = [self componentsSeparatedByRegex:@"[^a-zA-Z0-9]+"];
    if ([components count] == 0) {
        return selfLikeQuery;
    }
    
    NSMutableArray * queries = [NSMutableArray array];
    for (NSString *part in components) {
        if (part.length > 0) {
            [queries addObject:[part toLikeQueryFromValidSqlStringForAttribute:attribute]];
        }
    }
    if ([queries count] == 0) {
        return selfLikeQuery;
    }
    
    NSString * queryString = [queries componentsJoinedByString:@" AND "];
    return [NSString stringWithFormat:@"((%@) OR %@)", queryString, selfLikeQuery];
}

#pragma mark - Like queries for data models

- (NSString *)toLikeQueryForListing
{
    // Fuzzy match listings' attributes (custom fields)
    //
    // Get all child listings, check whether these listings have custom fields mathced
    //
    NSString * queryOfListingAttributeFuzzyMatching =
        [NSString stringWithFormat:@"((SELECT COUNT(*) from FCCustomField as customField WHERE customField.productID = listing.productID AND (%@ OR %@)) != 0)",
         [self toLikeQueryFromValidSqlStringForAttribute:@"customField.fieldName"],
         [self toLikeQueryFromValidSqlStringForAttribute:@"customField.fieldValue"]];
    
    // 1. For each listing, fuzzy match it's all child listings' name, ean13, upc parts
    //      to get the target
    NSString * queryOfChildListingsFuzzyMatching =
        [NSString stringWithFormat:@"((SELECT COUNT(*) from $T as listing WHERE ((listing.parentID = $T.productID AND $T.isParent == 1) OR (listing.productID = $T.productID AND $T.isParent != 1)) AND (%@ OR %@ OR %@ OR %@ OR %@ OR %@)) != 0)",
         [self toDepthMatchingLikeQueryForAttribute:@"listing.name"],
         [self toLikeQueryFromValidSqlStringForAttribute:@"listing.ean13"],
         [self toLikeQueryFromValidSqlStringForAttribute:@"listing.upc"],
         [self toLikeQueryFromValidSqlStringForAttribute:@"listing.barcode"],
         [self toLikeQueryFromValidSqlStringForAttribute:@"listing.secondaryBarcode"],
         queryOfListingAttributeFuzzyMatching];
    
    // 2. For each listing, fuzzy match it's listing supplier ID to get the target
    NSString * queryOfListingSupplierIDFuzzyMatching =
        [NSString stringWithFormat:@"((SELECT COUNT(*) from FCListingSupplier as supplier WHERE supplier.listingID = $T.listingID AND supplierID != 0 AND %@) != 0)",
            [self toLikeQueryFromValidSqlStringForAttribute:@"supplier.supplierProductID"]];
    
    return [NSString stringWithFormat:@"(%@ OR %@)",
            queryOfChildListingsFuzzyMatching,
            queryOfListingSupplierIDFuzzyMatching];
}

- (NSString *)toLikeQueryForSupplier
{
    return [NSString stringWithFormat:@"(%@ OR %@)",
            [self toDepthMatchingLikeQueryForAttribute:@"name"],
            [self toLikeQueryFromValidSqlStringForAttribute:@"phone"]];
}

- (NSString *)toLikeQueryForCustomerWithSpecifiedGiftCardNumber
{
    NSString *stringWithoutHypthen = [ [self toValidSqlString] stringByReplacingOccurrencesOfString:@"-" withString:@""];
    return [NSString stringWithFormat:@"(customerID in (SELECT customerID from FCGiftCard where cardNumber LIKE \"%%%@%%\") )",  stringWithoutHypthen];
}

- (NSString *)toLikeQueryForCustomer
{
    return [NSString stringWithFormat:@"(%@ OR %@ OR %@ OR %@ OR %@)",
            [self toDepthMatchingLikeQueryForAttribute:@"name"],
            [self toLikeQueryFromValidSqlStringForPhoneAttribute:@"phone"],
            [self toLikeQueryFromValidSqlStringForPhoneAttribute:@"homePhone"],
            [self toLikeQueryFromValidSqlStringForAttribute:@"notes"],
            [self toLikeQueryForCustomerWithSpecifiedGiftCardNumber]
           ];
}

- (NSString *)toLikeQueryForCustomerCreditCard
{
    return [self toDepthMatchingLikeQueryForAttribute:@"holder"];
}

- (NSString *)toLikeQueryForDepartment
{
    return [self toDepthMatchingLikeQueryForAttribute:@"name"];
}

- (NSString *)toLikeQueryForDiscount
{
    return [self toDepthMatchingLikeQueryForAttribute:@"name"];
}

@end
