//
//  NSString+NSPredicate.h
//  Bindo POS
//
//  Created by iwill on 2013-01-06.
//  Copyright (c) 2013年 BindoLabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSPredicate)

- (NSString *)predicateExpressionString;
+ (NSString *)predicateExpressionStringFromKeyword:(NSString *)keyword;

@end
