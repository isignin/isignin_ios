//
//  UIImage+BPExtentation.m
//  Bindo POS
//
//  Created by Kjuly on 10/17/13.
//  Copyright (c) 2013 BindoLabs. All rights reserved.
//

#import "UIImage+Extentation.h"

@implementation UIImage (Extentation)

- (UIImage *)tintWithColor:(UIColor *)tintColor
{
    UIGraphicsBeginImageContextWithOptions(self.size, NO, [[UIScreen mainScreen] scale]);
    CGRect drawRect = CGRectMake(0, 0, self.size.width, self.size.height);
    [self drawInRect:drawRect];
    [tintColor set];
    UIRectFillUsingBlendMode(drawRect, kCGBlendModeSourceAtop);
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return tintedImage;
}

@end
