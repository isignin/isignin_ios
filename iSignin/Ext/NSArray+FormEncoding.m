//
//  NSArray+FormEncoding.m
//  Bindo POS
//
//  Created by Allen on 12/4/13.
//  Copyright (c) 2013 BindoLabs. All rights reserved.
//

#import "NSArray+FormEncoding.h"
#import "RegexKitLite.h"
#import "NSString+HTTPHelper.h"

@implementation NSArray (FormEncoding)
- (NSString *)formEncodedString {
    NSMutableArray *encodedParams = [NSMutableArray arrayWithCapacity:self.count];
    for (NSString *p in self) {
        NSArray *parts = [[p stringByReplacingOccurrencesOfRegex:@"\n\r" withString:@"" ] captureComponentsMatchedByRegex:@"(.*?)=(.*)"];
        NSArray *newParts = @[parts[1], [NSString URLEncodedString:parts[2]]];
        [encodedParams addObject:[newParts componentsJoinedByString:@"="]];
    }
    return [encodedParams componentsJoinedByString:@"&" ];
}

- (NSData *)formEncodedData {
    return [[self formEncodedString] dataUsingEncoding: NSUTF8StringEncoding];
}
@end
