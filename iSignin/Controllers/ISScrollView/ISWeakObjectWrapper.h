//
//  ISWeakObjectWrapper.h
//  iSignin
//
//  Created by Damon Yuan on 28/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ISWeakObjectWrapper : NSProxy

- (instancetype)initWithObject:(id)object;

@end
