//
//  ISEasingFunction.h
//  iSignin
//
//  Created by Damon Yuan on 28/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ISEasingFunctionProtocol.h"

typedef NS_ENUM(NSUInteger, ISEasingFunctionType)
{
    ISEasingFunctionTypeLinear,
    
    ISEasingFunctionTypeQuadraticEaseIn,
    ISEasingFunctionTypeQuadraticEaseOut,
    ISEasingFunctionTypeQuadraticEaseInOut,
    
    ISEasingFunctionTypeCubicEaseIn,
    ISEasingFunctionTypeCubicEaseOut,
    ISEasingFunctionTypeCubicEaseInOut,
    
    ISEasingFunctionTypeQuarticEaseIn,
    ISEasingFunctionTypeQuarticEaseOut,
    ISEasingFunctionTypeQuarticEaseInOut,
    
    ISEasingFunctionTypeQuinticEaseIn,
    ISEasingFunctionTypeQuinticEaseOut,
    ISEasingFunctionTypeQuinticEaseInOut,
    
    ISEasingFunctionTypeSineEaseIn,
    ISEasingFunctionTypeSineEaseOut,
    ISEasingFunctionTypeSineEaseInOut,
    
    ISEasingFunctionTypeCircularEaseIn,
    ISEasingFunctionTypeCircularEaseOut,
    ISEasingFunctionTypeCircularEaseInOut,
    
    ISEasingFunctionTypeExponentialEaseIn,
    ISEasingFunctionTypeExponentialEaseOut,
    ISEasingFunctionTypeExponentialEaseInOut,
    
    ISEasingFunctionTypeElasticEaseIn,
    ISEasingFunctionTypeElasticEaseOut,
    ISEasingFunctionTypeElasticEaseInOut,
    
    ISEasingFunctionTypeBackEaseIn,
    ISEasingFunctionTypeBackEaseOut,
    ISEasingFunctionTypeBackEaseInOut,
    
    ISEasingFunctionTypeBounceEaseIn,
    ISEasingFunctionTypeBounceEaseOut,
    ISEasingFunctionTypeBounceEaseInOut
};

@interface ISEasingFunction : NSObject <ISEasingFunctionProtocol>

@property (nonatomic, assign) ISEasingFunctionType type;

+ (instancetype)easingFunctionWithType:(ISEasingFunctionType)type;

- (instancetype)initWithType:(ISEasingFunctionType)type;

@end
