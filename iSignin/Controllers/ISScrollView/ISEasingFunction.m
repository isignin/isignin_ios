//
//  ISEasingFunction.m
//  iSignin
//
//  Created by Damon Yuan on 28/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISEasingFunction.h"
#import "easing.h"

@interface ISEasingFunction ()

@property (nonatomic, assign) AHEasingFunction easingFunction;

@end

@implementation ISEasingFunction

+ (instancetype)easingFunctionWithType:(ISEasingFunctionType)type
{
    return [[self alloc] initWithType:type];
}

- (instancetype)initWithType:(ISEasingFunctionType)type
{
    if (self = [super init]) {
        [self setType:type];
    }
    return self;
}

- (CGFloat)solveForInput:(CGFloat)input
{
    return self.easingFunction(input);
}

- (void)setType:(ISEasingFunctionType)type
{
    static const NSDictionary *typeToFunctionMap;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        typeToFunctionMap = @{@(ISEasingFunctionTypeLinear): [NSValue valueWithPointer:LinearInterpolation],
                              
                              @(ISEasingFunctionTypeQuadraticEaseIn): [NSValue valueWithPointer:QuadraticEaseIn],
                              @(ISEasingFunctionTypeQuadraticEaseOut): [NSValue valueWithPointer:QuadraticEaseOut],
                              @(ISEasingFunctionTypeQuadraticEaseInOut): [NSValue valueWithPointer:QuadraticEaseInOut],
                              
                              @(ISEasingFunctionTypeCubicEaseIn): [NSValue valueWithPointer:CubicEaseIn],
                              @(ISEasingFunctionTypeCubicEaseOut): [NSValue valueWithPointer:CubicEaseOut],
                              @(ISEasingFunctionTypeCubicEaseInOut): [NSValue valueWithPointer:CubicEaseInOut],
                              
                              @(ISEasingFunctionTypeQuarticEaseIn): [NSValue valueWithPointer:QuarticEaseIn],
                              @(ISEasingFunctionTypeQuarticEaseOut): [NSValue valueWithPointer:QuarticEaseOut],
                              @(ISEasingFunctionTypeQuarticEaseInOut): [NSValue valueWithPointer:QuarticEaseInOut],
                              
                              @(ISEasingFunctionTypeQuinticEaseIn): [NSValue valueWithPointer:QuinticEaseIn],
                              @(ISEasingFunctionTypeQuinticEaseOut): [NSValue valueWithPointer:QuinticEaseOut],
                              @(ISEasingFunctionTypeQuinticEaseInOut): [NSValue valueWithPointer:QuinticEaseInOut],
                              
                              @(ISEasingFunctionTypeSineEaseIn): [NSValue valueWithPointer:SineEaseIn],
                              @(ISEasingFunctionTypeSineEaseOut): [NSValue valueWithPointer:SineEaseOut],
                              @(ISEasingFunctionTypeSineEaseInOut): [NSValue valueWithPointer:SineEaseInOut],
                              
                              @(ISEasingFunctionTypeCircularEaseIn): [NSValue valueWithPointer:CircularEaseIn],
                              @(ISEasingFunctionTypeCircularEaseOut): [NSValue valueWithPointer:CircularEaseOut],
                              @(ISEasingFunctionTypeCircularEaseInOut): [NSValue valueWithPointer:CircularEaseInOut],
                              
                              @(ISEasingFunctionTypeExponentialEaseIn): [NSValue valueWithPointer:ExponentialEaseIn],
                              @(ISEasingFunctionTypeExponentialEaseOut): [NSValue valueWithPointer:ExponentialEaseOut],
                              @(ISEasingFunctionTypeExponentialEaseInOut): [NSValue valueWithPointer:ExponentialEaseInOut],
                              
                              @(ISEasingFunctionTypeElasticEaseIn): [NSValue valueWithPointer:ElasticEaseIn],
                              @(ISEasingFunctionTypeElasticEaseOut): [NSValue valueWithPointer:ElasticEaseOut],
                              @(ISEasingFunctionTypeElasticEaseInOut): [NSValue valueWithPointer:ElasticEaseInOut],
                              
                              @(ISEasingFunctionTypeBackEaseIn): [NSValue valueWithPointer:BackEaseIn],
                              @(ISEasingFunctionTypeBackEaseOut): [NSValue valueWithPointer:BackEaseOut],
                              @(ISEasingFunctionTypeBackEaseInOut): [NSValue valueWithPointer:BackEaseInOut],
                              
                              @(ISEasingFunctionTypeBounceEaseIn): [NSValue valueWithPointer:BounceEaseIn],
                              @(ISEasingFunctionTypeBounceEaseOut): [NSValue valueWithPointer:BounceEaseOut],
                              @(ISEasingFunctionTypeBounceEaseInOut): [NSValue valueWithPointer:BounceEaseInOut]};
    });
    
    self.easingFunction = [typeToFunctionMap[@(type)] pointerValue];
    _type = type;
}

@end
