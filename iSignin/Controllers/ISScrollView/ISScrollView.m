//
//  ISScrollView.m
//  iSignin
//
//  Created by Damon Yuan on 28/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISScrollView.h"
#import "ISEasingFunctionProtocol.h"
#import "ISWeakObjectWrapper.h"

@interface UIView (ISFindFirstResponder)

- (id)isFindFirstResponder;

@end

@implementation UIView (SCFindFirstResponder)

- (id)isFindFirstResponder
{
    if (self.isFirstResponder) {
        return self;
    }
    
    for (UIView *subview in self.subviews) {
        id responder = [subview isFindFirstResponder];
        
        if (responder) {
            return responder;
        }
    }
    
    return nil;
}

@end

@interface ISScrollView ()

@property (nonatomic, strong) CADisplayLink *displayLink;
@property (nonatomic, strong) id<ISEasingFunctionProtocol> easingFunction;
@property (nonatomic, assign) CFTimeInterval duration;
@property (nonatomic, assign) CFTimeInterval startTime;
@property (nonatomic, assign) CGPoint startContentOffset;
@property (nonatomic, assign) CGPoint deltaContentOffset;
@property (nonatomic, copy) void(^animationCompletionBlock)();

@end

@implementation ISScrollView

- (void)dealloc
{
    [self.displayLink invalidate];
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.maximumNumberOfTouches = NSUIntegerMax;
    }
    return self;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    is_dispatch_main_async(^{
        [[NSNotificationCenter defaultCenter] postNotificationName:kISWelcomeViewShouldResetTimer
                                                            object:nil];
    })

    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        if (gestureRecognizer.numberOfTouches > self.maximumNumberOfTouches) {
            return NO;
        }
        
        CGPoint touchPoint = [gestureRecognizer locationInView:self];
        return ![self.touchRefusalArea containsPoint:touchPoint];
    }
    
    return YES;
}

#pragma mark - Public

- (BOOL)isRunningAnimation
{
    return (self.displayLink && ! self.displayLink.paused);
}

- (void)setContentOffset:(CGPoint)contentOffset
          easingFunction:(id<ISEasingFunctionProtocol>)easingFunction
                duration:(CFTimeInterval)duration
              completion:(void (^)())completion
{
    if (self.animationCompletionBlock) {
        self.animationCompletionBlock();
        self.animationCompletionBlock = nil;
    }
    
    if (CGPointEqualToPoint(self.contentOffset, contentOffset) || duration == 0.0f) {
        self.contentOffset = contentOffset;
        if (completion) {
            completion();
        }
    }
    
    if (self.displayLink == nil) {
        ISWeakObjectWrapper *target = [[ISWeakObjectWrapper alloc] initWithObject:self];
        self.displayLink = [CADisplayLink displayLinkWithTarget:target selector:@selector(_updateContentOffset:)];
        [self.displayLink setFrameInterval:1];
        [self.displayLink setPaused:YES];
        [self.displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    }
    
    self.duration = duration;
    self.easingFunction = easingFunction;
    self.animationCompletionBlock = completion;
    self.deltaContentOffset = CGPointMake(contentOffset.x - self.contentOffset.x, contentOffset.y - self.contentOffset.y);
    self.startTime = 0.0f;
    
    [self.displayLink setPaused:NO];
}

- (void)stopRunningAnimation
{
    if (self.displayLink.isPaused) {
        return;
    }
    
    [self.displayLink setPaused:YES];
    
    if ([self.delegate respondsToSelector:@selector(scrollViewDidEndScrollingAnimation:)]) {
        [self.delegate scrollViewDidEndScrollingAnimation:self];
    }
    
    if (self.animationCompletionBlock) {
        self.animationCompletionBlock();
        self.animationCompletionBlock = nil;
    }
}

#pragma mark - Private

- (void)_updateContentOffset:(CADisplayLink *)displayLink
{
    if (self.startTime == 0.0f) {
        self.startTime = self.displayLink.timestamp;
        self.startContentOffset = self.contentOffset;
        return;
    }
    
    CGFloat ratio = (CGFloat)((displayLink.timestamp - self.startTime) / self.duration);
    ratio = (1.0f - ratio < 0.01f) ? 1.0f : [self.easingFunction solveForInput:ratio];
    
    self.contentOffset = CGPointMake(self.startContentOffset.x + (self.deltaContentOffset.x * ratio),
                                     self.startContentOffset.y + (self.deltaContentOffset.y * ratio));
    
    if (ratio == 1.0f) {
        [self.displayLink setPaused:YES];
        
        if ([self.delegate respondsToSelector:@selector(scrollViewDidEndScrollingAnimation:)]) {
            [self.delegate scrollViewDidEndScrollingAnimation:self];
        }
        
        if (self.animationCompletionBlock) {
            self.animationCompletionBlock();
            self.animationCompletionBlock = nil;
        }
    }
}

- (void)scrollRectToVisible:(CGRect)rect animated:(BOOL)animated
{
    // Ignoring calls from any textField contained by this scrollview
    id firstResponder = [self isFindFirstResponder];
    if ([firstResponder isKindOfClass:[UITextField class]] || [firstResponder isKindOfClass:[UISearchBar class]]) {
        return;
    }
    
    [super scrollRectToVisible:rect animated:animated];
}

@end
