//
//  ISScrollView.h
//  iSignin
//
//  Created by Damon Yuan on 28/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ISEasingFunctionProtocol;

@interface ISScrollView : UIScrollView

@property (nonatomic, strong) UIBezierPath *touchRefusalArea;
@property (nonatomic, assign) NSUInteger maximumNumberOfTouches;

- (void)setContentOffset:(CGPoint)contentOffset
          easingFunction:(id<ISEasingFunctionProtocol>)easingFunction
                duration:(CFTimeInterval)duration
              completion:(void(^)())completion;
- (BOOL)isRunningAnimation;
- (void)stopRunningAnimation;

@end
