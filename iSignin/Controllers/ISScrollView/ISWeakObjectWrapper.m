//
//  ISWeakObjectWrapper.m
//  iSignin
//
//  Created by Damon Yuan on 28/3/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISWeakObjectWrapper.h"

@interface ISWeakObjectWrapper ()

@property (nonatomic, weak) id object;

@end

@implementation ISWeakObjectWrapper

- (instancetype)initWithObject:(id)object
{
    _object = object;
    return self;
}

- (id)forwardingTargetForSelector:(SEL)aSelector
{
    return self.object;
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector
{
    NSMethodSignature *methodSignature;
    
    id object = self.object;
    if (object) {
        methodSignature = [object methodSignatureForSelector:aSelector];
    } else {
        NSString *types = [NSString stringWithFormat:@"%s%s", @encode(id), @encode(SEL)];
        methodSignature = [NSMethodSignature signatureWithObjCTypes:[types UTF8String]];
    }
    return methodSignature;
}

- (void)forwardInvocation:(NSInvocation *)anInvocation
{
    [anInvocation invokeWithTarget:self.object];
}

@end
