//
//  ISShimmerLogoView.m
//  iSignin
//
//  Created by Damon Yuan on 1/4/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISShimmerLogoView.h"
#import "FBShimmeringView.h"
#import "ISLogoLabel.h"

@implementation ISShimmerLogoView

- (instancetype)init
{
    if (self = [super init]) {
        FBShimmeringView *shimmeringView = [[FBShimmeringView alloc] init];
        [self addSubview:shimmeringView];
        
        ISLogoLabel *logoLabel                   = [[ISLogoLabel alloc] init];
        logoLabel.textAlignment                  = NSTextAlignmentCenter;
        shimmeringView.contentView               = logoLabel;
        shimmeringView.shimmering                = YES;
        shimmeringView.shimmeringSpeed           = 100.f;
        shimmeringView.shimmeringDirection       = FBShimmerDirectionLeft;
        shimmeringView.shimmeringHighlightLength = 0.2f;
        
        NSLayoutConstraint *constraint;
        constraint = [NSLayoutConstraint constraintWithItem:shimmeringView
                                                  attribute:NSLayoutAttributeTop
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self
                                                  attribute:NSLayoutAttributeTop
                                                 multiplier:1.f
                                                   constant:0.f];
        [self addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:shimmeringView
                                                  attribute:NSLayoutAttributeBottom
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self
                                                  attribute:NSLayoutAttributeBottom
                                                 multiplier:1.f
                                                   constant:0.f];
        [self addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:shimmeringView
                                                  attribute:NSLayoutAttributeLeading
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self
                                                  attribute:NSLayoutAttributeLeading
                                                 multiplier:1.f
                                                   constant:0.f];
        [self addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:shimmeringView
                                                  attribute:NSLayoutAttributeTrailing
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self
                                                  attribute:NSLayoutAttributeTrailing
                                                 multiplier:1.f
                                                   constant:0.f];
        [self addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:logoLabel
                                                  attribute:NSLayoutAttributeTop
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:shimmeringView
                                                  attribute:NSLayoutAttributeTop
                                                 multiplier:1.f
                                                   constant:0.f];
        [shimmeringView addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:logoLabel
                                                  attribute:NSLayoutAttributeBottom
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:shimmeringView
                                                  attribute:NSLayoutAttributeBottom
                                                 multiplier:1.f
                                                   constant:0.f];
        [shimmeringView addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:logoLabel
                                                  attribute:NSLayoutAttributeLeading
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:shimmeringView
                                                  attribute:NSLayoutAttributeLeading
                                                 multiplier:1.f
                                                   constant:0.f];
        [shimmeringView addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:logoLabel
                                                  attribute:NSLayoutAttributeTrailing
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:shimmeringView
                                                  attribute:NSLayoutAttributeTrailing
                                                 multiplier:1.f
                                                   constant:0.f];
        [shimmeringView addConstraint:constraint];
        
        [shimmeringView setTranslatesAutoresizingMaskIntoConstraints:NO];
        [logoLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [self setNeedsUpdateConstraints];
    }
    return self;
}

- (instancetype)initWithFontSize:(CGFloat)fontSize
{
    if (self = [super init]) {
        FBShimmeringView *shimmeringView = [[FBShimmeringView alloc] init];
        [self addSubview:shimmeringView];
        
        ISLogoLabel *logoLabel                   = [[ISLogoLabel alloc] initWithFontSize:fontSize];
        logoLabel.textAlignment                  = NSTextAlignmentCenter;
        shimmeringView.contentView               = logoLabel;
        shimmeringView.shimmering                = YES;
        shimmeringView.shimmeringSpeed           = 100.f;
        shimmeringView.shimmeringDirection       = FBShimmerDirectionLeft;
        shimmeringView.shimmeringHighlightLength = 0.2f;
        
        NSLayoutConstraint *constraint;
        constraint = [NSLayoutConstraint constraintWithItem:shimmeringView
                                                  attribute:NSLayoutAttributeTop
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self
                                                  attribute:NSLayoutAttributeTop
                                                 multiplier:1.f
                                                   constant:0.f];
        [self addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:shimmeringView
                                                  attribute:NSLayoutAttributeBottom
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self
                                                  attribute:NSLayoutAttributeBottom
                                                 multiplier:1.f
                                                   constant:0.f];
        [self addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:shimmeringView
                                                  attribute:NSLayoutAttributeLeading
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self
                                                  attribute:NSLayoutAttributeLeading
                                                 multiplier:1.f
                                                   constant:0.f];
        [self addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:shimmeringView
                                                  attribute:NSLayoutAttributeTrailing
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self
                                                  attribute:NSLayoutAttributeTrailing
                                                 multiplier:1.f
                                                   constant:0.f];
        [self addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:logoLabel
                                                  attribute:NSLayoutAttributeTop
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:shimmeringView
                                                  attribute:NSLayoutAttributeTop
                                                 multiplier:1.f
                                                   constant:0.f];
        [shimmeringView addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:logoLabel
                                                  attribute:NSLayoutAttributeBottom
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:shimmeringView
                                                  attribute:NSLayoutAttributeBottom
                                                 multiplier:1.f
                                                   constant:0.f];
        [shimmeringView addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:logoLabel
                                                  attribute:NSLayoutAttributeLeading
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:shimmeringView
                                                  attribute:NSLayoutAttributeLeading
                                                 multiplier:1.f
                                                   constant:0.f];
        [shimmeringView addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:logoLabel
                                                  attribute:NSLayoutAttributeTrailing
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:shimmeringView
                                                  attribute:NSLayoutAttributeTrailing
                                                 multiplier:1.f
                                                   constant:0.f];
        [shimmeringView addConstraint:constraint];
        
        [shimmeringView setTranslatesAutoresizingMaskIntoConstraints:NO];
        [logoLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [self setNeedsUpdateConstraints];
    }
    return self;
}

@end
