//
//  ISLogoLabel.h
//  iSignin
//
//  Created by Damon Yuan on 1/4/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISLogoLabel : UILabel

- (instancetype)initWithFontSize:(CGFloat)fontSize;

@end
