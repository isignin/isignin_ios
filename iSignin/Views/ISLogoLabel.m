//
//  ISLogoLabel.m
//  iSignin
//
//  Created by Damon Yuan on 1/4/15.
//  Copyright (c) 2015 Damon Yuan. All rights reserved.
//

#import "ISLogoLabel.h"

@implementation ISLogoLabel

- (instancetype)init
{
    if (self = [super init]) {
        NSString *string = @" iSignin ";
        
        NSShadow *shadow    = [[NSShadow alloc] init];
        shadow.shadowColor  = [UIColor silverColor];
        shadow.shadowOffset = CGSizeMake(2.f, 2.f);
        shadow.shadowBlurRadius = 0.f;
        
        NSDictionary *attributesForLogo = @{NSFontAttributeName : [UIFont boldFlatFontOfSize:45.f],
                                            NSForegroundColorAttributeName : [UIColor whiteColor],
                                            NSBackgroundColorAttributeName : [UIColor alizarinColor],
                                            NSShadowAttributeName : shadow};
        
        NSAttributedString *result = [[NSAttributedString alloc] initWithString:string
                                                                     attributes:attributesForLogo];
        self.attributedText = result;
        self.backgroundColor = [UIColor clearColor];
        [self sizeToFit];
        self.adjustsFontSizeToFitWidth = YES;
        self.textAlignment = NSTextAlignmentCenter;
    }
    return self;
}

- (instancetype)initWithFontSize:(CGFloat)fontSize
{
    if (self = [super init]) {
        NSString *string = @" iSignin ";
        
        NSShadow *shadow    = [[NSShadow alloc] init];
        shadow.shadowColor  = [UIColor silverColor];
        shadow.shadowOffset = CGSizeMake(2.f, 2.f);
        shadow.shadowBlurRadius = 0.f;
        
        NSDictionary *attributesForLogo = @{NSFontAttributeName : [UIFont boldFlatFontOfSize:fontSize],
                                            NSForegroundColorAttributeName : [UIColor whiteColor],
                                            NSBackgroundColorAttributeName : [UIColor alizarinColor],
                                            NSShadowAttributeName : shadow};
        
        NSAttributedString *result = [[NSAttributedString alloc] initWithString:string
                                                                     attributes:attributesForLogo];
        self.attributedText = result;
        self.backgroundColor = [UIColor clearColor];
        [self sizeToFit];
        self.adjustsFontSizeToFitWidth = YES;
        self.textAlignment = NSTextAlignmentCenter;
    }
    return self;
}

@end
